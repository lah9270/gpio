/* 
 * File:   interruptHandler.h
 * Author: Derek Gutheil <dkg8689@rit.edu>
 *
 * Created on September 8, 2016
 */

#ifndef INTERRUPTHANDLER_H
#define    INTERRUPTHANDLER_H

/* Turns on global and peripheral interrupts */
void init_interruptHandler();

/* DO NOT CALL. Automatically called by the processor whenever any interrupt occurs */
void interrupt interruptHandler();

#endif    /* INTERRUPTHANDLER_H */

