/* 
 * File:   interruptHandler.c
 * Author: Derek Gutheil <dkg8689@rit.edu>
 *
 * Created on September 8, 2016
 */

#include "interruptHandler.h"
#include "taskManager.h"
#include "MC_CAN.h"
#include "xc.h"
#include "timers.h"

void init_interruptHandler()
{
    //Enable Peripheral Interrupts
    INTCONbits.PEIE = 1;
    //Enable Global Interrupts
    INTCONbits.GIE = 1;
}

void interrupt interruptHandler()
{
    //Timer4 trigger interrupt flag
    if (TMR4IE && TMR4IF)
    {
        TMR4IF = 0; //turn off triggered flag
        taskManager();
    }

    //CAN RX Buffer interrupt flag
    if (RXBnIE && RXB0IF)
    {
        RXB0IF = 0; //turn off triggered flag
        mc_can_getRXBuffer0(); //Handle the message
    }
    
    //CAN RX Buffer 1 interrupt flag
    if (RXBnIE && RXB1IF)
    {
        RXB1IF = 0; //turn off triggered flag
        mc_can_getRXBuffer1(); //Handle the message
    }
}