/*
 * File:    MC_SPI.c
 * Created: 3/24/2017
*/

#include "MC_SPI.h"
#include "MC_processor.h"
#include "spi.h"

/* ========================================================================
 * Private Macro Definitions
 * ========================================================================
*/

/* ===========================================================================================================================
 * Private module variables
 * ===========================================================================================================================
*/
mc_spi_sync_modes_E spiRequestedSyncMode;
mc_spi_bus_modes_E spiRequestedBusMode;
mc_spi_sample_modes_E spiRequestedSampleMode;

/* ===========================================================================================================================
 * Private module function declarations
 * ===========================================================================================================================
*/

/* ===========================================================================================================================
 *  Module function definitions
 * ===========================================================================================================================
*/
void mc_spi_setRequestedSyncMode(mc_spi_sync_modes_E syncMode)
{
    spiRequestedSyncMode = syncMode;
}

void mc_spi_setRequestedBusMode(mc_spi_bus_modes_E busMode)
{
    spiRequestedBusMode = busMode;
}

void mc_spi_setRequestedSampleMode(mc_spi_sample_modes_E sampleMode)
{
    spiRequestedSampleMode = sampleMode;
}

void mc_spi_open()
{
    OpenSPI((uint8_t)spiRequestedSyncMode, (uint8_t)spiRequestedBusMode, (uint8_t)spiRequestedSampleMode);
}

void mc_spi_close()
{
    // reset request variables to their default states
    spiRequestedSyncMode = MC_SPI_SYNC_DEFAULT;
    spiRequestedBusMode = MC_SPI_MODE_DEFAULT;
    spiRequestedSampleMode = MC_SPI_SAMPLE_DEFAULT;
    
    //close the SPI port
    CloseSPI();
}

void mc_spi_reopen(mc_spi_sync_modes_E syncMode, mc_spi_bus_modes_E busMode, mc_spi_sample_modes_E sampleMode)
{
    /* We probably don't need to actually close it, can probably just call open with new settings */
    mc_spi_close();
    spiRequestedSyncMode = syncMode;
    spiRequestedBusMode = busMode;
    spiRequestedSampleMode = sampleMode;
    mc_spi_open();
}


uint8_t mc_spi_readChar()
{
    return ReadSPI();
}

int8_t mc_spi_writeChar(uint8_t data_out)
{
    return WriteSPI(data_out);
}