/*
 * File:    MC_CAN.c
 * Date:    1/7/17
 */

#include "MC_CAN.h"
#include "MC_board.h"
#include "MC_processor.h"
#include <string.h> // for memcpy())

/* =============================================================================
 * Private Module constant definitions
 * =============================================================================
 */

/* By definition, the Nominal Bit Time is programmable from a minimum of 8 TQ to a maximum of 25 TQ.
 * A value of 16 seems pretty standard 
 */
#define CAN_TIME_QUANTA_MULTIPLIER_16 (16)
#define CAN_TIME_QUANTA_MULTIPLIER_8 (8)

/* The Sync Jump Width could be the maximum of 4 TQ. However, normally a large SJW is
 * only necessary when the clock generation of the different nodes is inaccurate or unstable, such as using
 * ceramic resonators. Typically, an SJW of 1 is enough.
 * We picked 2 as a seemingly safe value
 * This bit is stored offset by 1 (See datasheet pg. 430)
 */
#define CAN_SYNC_JUMP_WIDTH ((2) - 1)

/* See page 446 of the datasheet for details on where this equation came from */
#define CAN_BAUD_RATE_PRESCALER(can_bitrate, time_quanta_multiplier) (((FREQUENCY)/(2*can_bitrate*time_quanta_multiplier)) - 1)


/* This part of the bit time is used to compensate for physical delay times within the network. These delay times
 * consist of the signal propagation time on the bus line and the internal delay time of the nodes. The length of
 * the propagation segment can be programmed from 1 TQ to 8 TQ by setting the PRSEG<2:0> bits.
 * This bit is stored offset by 1 (See datasheet pg. 431)
 */
#define CAN_PROPOGATION_SEGMENT ((2) - 1)

/* The SEG1PH bits set the length of Phase Segment 1 in TQ
 * This added to MC_CAN_PROPOGATION_SEGMENT needs to be greater than or equal to MC_CAN_PHASE_SEGMENT_2
 * This bit is stored offset by 1 (See datasheet pg. 431)
 */
#define CAN_PHASE_SEGMENT_1 ((8) - 1)

/* The SEG2PHTS bit controls how the length of Phase Segment 2 is determined. 
 * If this bit is set to a ?1?, then the length of Phase Segment 2 is determined by the SEG2PH bits of BRGCON3.
 * If the SEG2PHTS bit is set to a ?0?, then the length of Phase Segment 2 is the greater of Phase Segment 1
 * and the information processing time (which is fixed at 2 TQ for the PIC18F66K80 family).
 * 
 * 1 = programmable (BRGCON3.SEGPH)
 * 0 = max of PHEG1 or IPT, whichever is greater
 */
#define CAN_PHASE_SEGMENT_2_SELECT (1)

/*The SEG2PH<2:0> bits set the length (in TQ) of Phase Segment 2
 * Needs to be greater than or equal to than MC_CAN_SYNC_JUMP_WIDTH
 * Needs to be smaller than (MC_CAN_PROPOGATION_SEGMENT + MC_CAN_PHASE_SEGMENT_1)
 * This bit is stored offset by 1 (See datasheet pg. 432)
 */
#define CAN_PHASE_SEGMENT_2 ((5) - 1)

/* The SAM bit controls how many times the RXCAN pin is sampled. Setting this bit
 * to a ?1? causes the bus to be sampled three times: twice at TQ/2 before the sample point and once at the normal
 * sample point (which is at the end of Phase Segment 1). The value of the bus is determined to be the value read
 * during at least two of the samples. If the SAM bit is set to a ?0?, then the RXCAN pin is sampled only once at
 * the sample point.
 *
 * 1 = sample bus 3 times (2 time prior, and one at the sample point).
 * 0 = sample bus once at sample point.
 */
#define CAN_NUM_SAMPLES (0)

/* The WAKIF interrupt is the only module interrupt that is still active in the Disable/Sleep mode. If the WAKDIS is
 * cleared and WAKIE is set, the processor will receive an interrupt whenever the module detects recessive to
 * dominant transition. On wake-up, the module will automatically be set to the previous mode of operation. For
 * example, if the module was switched from Normal to Disable/Sleep mode on bus activity wake-up, the
 * module will automatically enter into Normal mode and the first message that caused the module to wake-up is
 * lost. The module will not generate any error frame. Firmware logic must detect this condition and make
 * sure that retransmission is requested. If the processor receives a wake-up interrupt while it is sleeping, more
 * than one message may get lost. The actual number of messages lost would depend on the processor
 * oscillator start-up time and incoming message bit rate. The TXCAN pin will stay in the recessive state while the
 * module is in Disable/Sleep mode.
 *
 * 1 = Disable CAN bus activity wake-up feature.
 * 0 = Enable CAN bus activity wake-up feature.
 */
#define CAN_WAKE_UP_DISABLED (1)

/* Selects CAN bus Line Filter for Wake-up bit
 * 1 = Use CAN bus line filter for wake-up
 * 0 = CAN bus line filter is not used for wake-up
 */
#define CAN_WAKE_UP_FILTER (1)

/* CLKSEL: CAN Clock Source Selection bit
 * 1 = Use the oscillator as the source of the CAN system clock
 * 0 = Use the PLL as the source of the CAN system clock
 */
#define CAN_CLOCK_SOURCE_OSCILLATOR (1)

/* RB0DBEN: Receive Buffer 0 Double-Buffer Enable bit
 * 1 = Receive Buffer 0 overflow will write to Receive Buffer 1
 * 0 = No Receive Buffer 0 overflow to Receive Buffer 1 
 */
#define CAN_RECEIVE_BUFFER_0_OVERFLOW_DISABLE (0)
#define CAN_RECEIVE_BUFFER_0_OVERFLOW_ENABLE  (1)

/* Mode 0:
 * RXM<1:0>: Receive Buffer Mode bit 1 (combines with RXM0 to form RXM<1:0> bits, see bit 5)
 * 11 = Receive all messages (including those with errors); filter criteria is ignored
 * 10 = Receive only valid messages with extended identifier; EXIDEN in RXFnSIDL must be ?1?
 * 01 = Receive only valid messages with standard identifier; EXIDEN in RXFnSIDL must be ?0?
 * 00 = Receive all valid messages as per the EXIDEN bit in the RXFnSIDL register
 * 
 * for now, '01'  is chosen
 */
#define CAN_RECEIVE_BUFFER_MODE_BIT1 (0)
#define CAN_RECEIVE_BUFFER_MODE_BIT0 (1)

/* Mode 0:
 * FILHIT<2:0>: Filter Hit bits
 * These bits indicate which acceptance filter enabled the last message reception into Receive Buffer 1.
 * 111 = Reserved
 * 110 = Reserved
 * 101 = Acceptance Filter 5 (RXF5)
 * 100 = Acceptance Filter 4 (RXF4)
 * 011 = Acceptance Filter 3 (RXF3)
 * 010 = Acceptance Filter 2 (RXF2)
 * 001 = Acceptance Filter 1 (RXF1), only possible when RXB0DBEN bit is set
 * 000 = Acceptance Filter 0 (RXF0), only possible when RXB0DBEN bit is set
 */
#define CAN_RECEIVE_FILTER_HIT_0 (0)

/* Receive acceptance filter value
 */
#define CAN_RECEIVE_ACCEPTANCE_MASK_ALL (0)

/* Interrupt control flags
 * See data sheet page 435:
 * 1 = Enable interrupt
 * 0 = Disable interrupt
 */
#define CAN_INTERRUPT_ENABLE (1)
#define CAN_INTERRUPT_DISABLE (0)

/* EXIDE: Extended Identifier Enable
 * 1 = Message will transmit extended ID, SID<10:0> become EID<28:18>
 * 0 = Message will transmit standard ID, EID<17:0> are ignored 
 * See page 401 of datasheet
 */
#define CAN_EXTENDED_ID (1)
#define CAN_STANDARD_ID (0)

/* TXRTR: Transmit Remote Frame Transmission Request
 * 1 = Transmitted message will have the TXRTR bit set
 * 0 = Transmitted message will have the TXRTR bit cleared
 * See page 403 of the datasheet
 */
#define CAN_TRANSMIT_REMOTE_FRAME_REQUEST (1)
#define CAN_TRANSMIT_REMOTE_FRAME_NO_REQUEST (0)

/* Max CAN receive buffer size
 * Needs to be big enough to handle all of the messages received via interrupt before the task manager can get the IO layer to take care of them
 * 1,000 kbits/sec * (1 byte/8 bits) * (1 sec / 1000 msec) * (1 message / 16 bytes) = ~8 messages/msec
 * Mathematically, we should be pretty safe with a fairly conservative value (8 should be fine)
 * We should start high and check it empirically
 */
#define CAN_RECEIVE_BUFFER_SIZE (16)

#define CAN_PACK_ADDRESS(low0_2, high3_10) ( (((unsigned int)high3_10) << 3) | (((unsigned int)low0_2) & (unsigned int)(0x7))  )
#define CAN_EXTRACT_ADDRESS_LOW0_2(ID) ( (unsigned char)(((unsigned int)ID) & ((unsigned int)0x7) ) )
#define CAN_EXTRACT_ADDRESS_HIGH3_10(ID) ( (unsigned char)(((unsigned int)ID) >> 3) )


/* ===========================================================================================================================
 * Private Enumerated type definitions
 * ===========================================================================================================================
 */

typedef enum can_register_E
{
    CAN_REGISTER_CLEAR  = 0u,
    CAN_REGISTER_SET    = 1u,        
} can_register_E;


/* CAN Operation Modes
 * 1xx = Configuration mode
 * 011 = Listen Only mode
 * 010 = Loopback mode
 * 001 = Disabled/Sleep mode
 * 000 = Normal mode 
 * See datasheet page 394/5
 */
typedef enum can_operating_mode_E
{
    CAN_NORMAL_MODE         = 0u,
    CAN_SLEEP_MODE          = 1u,
    CAN_LOOPBACK_MODE       = 2u,
    CAN_LISTEN_ONLY_MODE    = 3u,
    CAN_CONFIG_MODE         = 4u,
            
} can_operating_mode_E;


/* TXREQ: Transmit Request Status bit
 * 1 = Requests sending a message; clears the TXABT, TXLARB and TXERR bits
 * 0 = Automatically cleared when the message is successfully sent 
 */
typedef enum can_transmit_request_E
{
    CAN_BUFFER_NO_REQUEST = 0u,
    CAN_BUFFER_FINISHED_TRANSMITTING = 0u,
    CAN_BUFFER_REQUEST_TRANSMIT = 1u,
    CAN_BUFFER_TRANSMITTING = 1u,
} can_transmit_request_E;


/* ===========================================================================================================================
 * Private module variables
 * ===========================================================================================================================
 */
mc_can_frame_S canReceiveBuffer[CAN_RECEIVE_BUFFER_SIZE];
uint8_t canReceiveBufferIn;
uint8_t canReceiveBufferOut;

mc_can_receive_pending_S canPendingHWBuffers;
mc_can_mode_E canRequestedOperatingMode;
mc_can_baud_E canRequestedBaudrate;

/* ===========================================================================================================================
 * Private module function declarations
 * ===========================================================================================================================
 */

void can_setupCANPins();
void can_enterMode(can_operating_mode_E canMode);
void can_enableRecieveBufferInterrupts();
void can_enableTansmitBufferInterrupts();
void can_setBaudrate();
void can_setCANIOControlRegister();
void can_setEnhancedCANControlRegister();
void can_setRecieveBuffer0ControlRegister();
void can_setRecieveBuffer1ControlRegister();
void can_updatePendingHWBuffers();
void can_receiveBufferInIncriment();
void can_receiveBufferOutIncriment();

/* ===========================================================================================================================
 *  Module function definitions
 * ===========================================================================================================================
 */

void mc_can_setRequestedBaudrate(mc_can_baud_E baudrate)
{
    canRequestedBaudrate = baudrate;
}

void mc_can_setRequestedOperatingMode(mc_can_mode_E operatingMode)
{
    canRequestedOperatingMode = operatingMode;
}

void mc_can_init()
{
    //Initialize module variables
    canReceiveBufferIn = 0;
    canReceiveBufferOut = 0;
    canPendingHWBuffers.pending = 0;
    
    can_setupCANPins();
    can_enableRecieveBufferInterrupts();
    can_enableTansmitBufferInterrupts();
    can_enterMode(CAN_CONFIG_MODE);
    can_setBaudrate();
    can_setCANIOControlRegister();
    can_setEnhancedCANControlRegister();
    can_setRecieveBuffer0ControlRegister();
    can_setRecieveBuffer1ControlRegister();
    can_enterMode(CAN_NORMAL_MODE);
}

void can_setupCANPins()
{
    //Set CANTX to an output pin and set it low
    MC_pin_setDirection(MC_BOARD_CANTX, MC_PIN_OUTPUT);
    MC_GPIO_write(MC_BOARD_CANTX, MC_PIN_LOW);

    //Set CANRX to an input pin
    MC_pin_setDirection(MC_BOARD_CANRX, MC_PIN_INPUT);
}

void can_enterMode(can_operating_mode_E canMode)
{
    CANCONbits.REQOP = canMode;  /*Set Configuration Mode i.e. CANCON = '10XXXXXX'*/
    while(CANSTATbits.OPMODE != canMode){
        /* Wait until mode is set */
    }
}

void can_setBaudrate()
{
    uint24_t canBitrate;
    uint8_t timeQuantaMultiplier;
    int8_t baudRatePrescaler;
    
    switch (canRequestedBaudrate)
    {
        case MC_CAN_BAUD_125K:
        {
            canBitrate = 125000;
            timeQuantaMultiplier = CAN_TIME_QUANTA_MULTIPLIER_16;
            break;
        }
        
        case MC_CAN_BAUD_250K:
        {
            canBitrate = 250000;
            timeQuantaMultiplier = CAN_TIME_QUANTA_MULTIPLIER_16;
            break;
        }
        
        case MC_CAN_BAUD_500K:
        {
            canBitrate = 500000;
            timeQuantaMultiplier = CAN_TIME_QUANTA_MULTIPLIER_8;
            break;
        }
        
        case MC_CAN_BAUD_1M:
        {
            canBitrate = 1000000;
            //not possible without a clock speed increase!
            timeQuantaMultiplier = 0;
            break;
        }
        case MC_CAN_BAUD_DEFAULT:
        {
            canBitrate = 0;
            timeQuantaMultiplier = 0;
            break;
        }
    }
          
    baudRatePrescaler = CAN_BAUD_RATE_PRESCALER(canBitrate, timeQuantaMultiplier);
    
    BRGCON1bits.BRP = baudRatePrescaler; //0
    BRGCON1bits.SJW = CAN_SYNC_JUMP_WIDTH;

    BRGCON2bits.SEG2PHTS = CAN_PHASE_SEGMENT_2_SELECT;
    BRGCON2bits.SAM = CAN_NUM_SAMPLES;
    BRGCON2bits.SEG1PH = CAN_PHASE_SEGMENT_1;
    BRGCON2bits.PRSEG = CAN_PROPOGATION_SEGMENT;

    BRGCON3bits.WAKDIS = CAN_WAKE_UP_DISABLED;
    BRGCON3bits.WAKFIL = CAN_WAKE_UP_FILTER;
    BRGCON3bits.SEG2PH = CAN_PHASE_SEGMENT_2;
}

void can_setCANIOControlRegister()
{
    CIOCONbits.CLKSEL = CAN_CLOCK_SOURCE_OSCILLATOR;
}

void can_setEnhancedCANControlRegister()
{
    ECANCONbits.MDSEL = canRequestedOperatingMode;
}

void can_setRecieveBuffer0ControlRegister()
{
    /* Initialize RBX0CON*/
    //Turn the mask/filter on. 
    //This is needed to stop the the receive buffer from picking up the transmit's messages
    RXB0CONbits.RXM1 = CAN_RECEIVE_BUFFER_MODE_BIT1;
    RXB0CONbits.RXM0 = CAN_RECEIVE_BUFFER_MODE_BIT0;
    RXB0CONbits.RB0DBEN = CAN_RECEIVE_BUFFER_0_OVERFLOW_DISABLE;
    
    //set the mask to 0 to see everything on the bus
    RXM0SIDH = CAN_RECEIVE_ACCEPTANCE_MASK_ALL;
    RXM0SIDLbits.SID = CAN_RECEIVE_ACCEPTANCE_MASK_ALL;
}

void can_setRecieveBuffer1ControlRegister()
{
   /* Initialize RBX1CON */
    RXB1CONbits.RXM1 = CAN_RECEIVE_BUFFER_MODE_BIT1;
    RXB1CONbits.RXM0 = CAN_RECEIVE_BUFFER_MODE_BIT0;
    
    // DG: I don't know why we are setting this, seems to be a read only register...
    RXB1CONbits.FILHIT2 = CAN_RECEIVE_FILTER_HIT_0;  
    
    //set the mask to 0 to see everything on the bus
    /* same filter as RXB0CON */
    RXM1SIDH = CAN_RECEIVE_ACCEPTANCE_MASK_ALL;
    RXM1SIDLbits.SID = CAN_RECEIVE_ACCEPTANCE_MASK_ALL;
}

void can_enableRecieveBufferInterrupts()
{
    PIE5bits.RXB1IE = CAN_INTERRUPT_ENABLE;
    PIE5bits.RXB0IE = CAN_INTERRUPT_ENABLE;
}

void can_enableTansmitBufferInterrupts()
{
    PIE5bits.TXB0IE = CAN_INTERRUPT_ENABLE;
    PIE5bits.TXB1IE = CAN_INTERRUPT_ENABLE;
    PIE5bits.TXB2IE = CAN_INTERRUPT_ENABLE;
}

uint8_t mc_can_transmit(mc_can_frame_S * frame)
{
    uint8_t addr0_2;
    uint8_t addr3_10;
    
    uint8_t returnValue = 0;
    
    addr0_2 = CAN_EXTRACT_ADDRESS_LOW0_2(frame->address);
    addr3_10 = CAN_EXTRACT_ADDRESS_HIGH3_10(frame->address);
    
    
    /* Use Transmit Buffer 0 if it is free*/
    if (TXB0CONbits.TXREQ == CAN_BUFFER_NO_REQUEST) 
    {

        /* Transmit Buffer 0 Standard Identifier Registers*/
        TXB0SIDLbits.EXIDE = CAN_STANDARD_ID;        
        TXB0SIDLbits.SID = addr0_2;
        TXB0SIDHbits.SID = addr3_10;
        
        
        /* Transmit Buffer 0 Data Length Code Register*/
        TXB0DLCbits.TXRTR = CAN_TRANSMIT_REMOTE_FRAME_NO_REQUEST;
        TXB0DLCbits.DLC = frame->numBytes;

        /* Transmit Buffer 0 Data Field Registers*/
        TXB0D0 = frame->data[0];
        TXB0D1 = frame->data[1];
        TXB0D2 = frame->data[2];
        TXB0D3 = frame->data[3];
        TXB0D4 = frame->data[4];
        TXB0D5 = frame->data[5];
        TXB0D6 = frame->data[6];
        TXB0D7 = frame->data[7];

        /* Transmit Buffer 0 Control Register*/
        TXB0CONbits.TXPRI = frame->priority;
        TXB0CONbits.TXREQ = CAN_BUFFER_REQUEST_TRANSMIT;		
        
        returnValue = 1;
        
    /* Use Transmit Buffer 1 if it is free*/
    } 
    else if (TXB1CONbits.TXREQ == CAN_BUFFER_NO_REQUEST)
    {

        /* Transmit Buffer 1 Standard Identifier Registers*/
        TXB1SIDLbits.EXIDE = CAN_STANDARD_ID;        
        TXB1SIDLbits.SID = addr0_2;
        TXB1SIDHbits.SID = addr3_10;
        
        /* Transmit Buffer 1 Data Length Code Register*/
        TXB1DLCbits.TXRTR = CAN_TRANSMIT_REMOTE_FRAME_NO_REQUEST;
        TXB1DLCbits.DLC = frame->numBytes;

        /* Transmit Buffer 1 Data Field Registers*/
        TXB1D0 = frame->data[0];
        TXB1D1 = frame->data[1];
        TXB1D2 = frame->data[2];
        TXB1D3 = frame->data[3];
        TXB1D4 = frame->data[4];
        TXB1D5 = frame->data[5];
        TXB1D6 = frame->data[6];
        TXB1D7 = frame->data[7];

        /* Transmit Buffer 1 Control Register*/
        TXB1CONbits.TXPRI = frame->priority;
        TXB1CONbits.TXREQ = CAN_BUFFER_REQUEST_TRANSMIT;		
        
        returnValue = 1;
        
    /* Use Transmit Buffer 2 if it is free*/    
    }
    else if (TXB2CONbits.TXREQ == CAN_BUFFER_NO_REQUEST)
    {

        /* Transmit Buffer 2 Standard Identifier Registers*/
        TXB2SIDLbits.EXIDE = CAN_STANDARD_ID;        
        TXB2SIDLbits.SID = addr0_2;
        TXB2SIDHbits.SID = addr3_10;
        
        /* Transmit Buffer 2 Data Length Code Register*/
        TXB2DLCbits.TXRTR = CAN_TRANSMIT_REMOTE_FRAME_NO_REQUEST;
        TXB2DLCbits.DLC = frame->numBytes;

        /* Transmit Buffer 2 Data Field Registers*/
        TXB2D0 = frame->data[0];
        TXB2D1 = frame->data[1];
        TXB2D2 = frame->data[2];
        TXB2D3 = frame->data[3];
        TXB2D4 = frame->data[4];
        TXB2D5 = frame->data[5];
        TXB2D6 = frame->data[6];
        TXB2D7 = frame->data[7];

        /* Transmit Buffer 2 Control Register*/
        TXB2CONbits.TXPRI = frame->priority;
        TXB2CONbits.TXREQ = CAN_BUFFER_REQUEST_TRANSMIT;		
        
        returnValue = 1;
    }
    
    return returnValue;
}

void mc_can_getMessage(mc_can_frame_S * frameOut)
{
    frameOut->address = canReceiveBuffer[canReceiveBufferOut].address;
    frameOut->numBytes = canReceiveBuffer[canReceiveBufferOut].numBytes;
    frameOut->priority = canReceiveBuffer[canReceiveBufferOut].priority;
  
    memcpy(frameOut->data, canReceiveBuffer[canReceiveBufferOut].data, canReceiveBuffer[canReceiveBufferOut].numBytes ); 
    
    /* Since we just wrote all of the data out of CAN_receiveBuffer, increment the out index */
    can_receiveBufferOutIncriment();
}

uint8_t mc_can_bufferIsEmpty()
{
    return (canReceiveBufferOut == canReceiveBufferIn);
}

void can_updatePendingHWBuffers()
{
    canPendingHWBuffers.RXB0 = RXB0CONbits.RXFUL;
    canPendingHWBuffers.RXB1 = RXB1CONbits.RXFUL;
    if (canRequestedOperatingMode != MC_CAN_LEGACY_MODE)
    {
        canPendingHWBuffers.RXB2 = B0CONbits.RXFUL;
        canPendingHWBuffers.RXB3 = B1CONbits.RXFUL;
        canPendingHWBuffers.RXB4 = B2CONbits.RXFUL;
        canPendingHWBuffers.RXB5 = B3CONbits.RXFUL;
        canPendingHWBuffers.RXB6 = B4CONbits.RXFUL;
        canPendingHWBuffers.RXB7 = B5CONbits.RXFUL;
    }
}

void MC_CAN_getBuffers()
{
    can_updatePendingHWBuffers();
    if (canPendingHWBuffers.pending != 0)
    {
        if (canPendingHWBuffers.RXB0)
        {
            mc_can_getRXBuffer0();
        }
        if (canPendingHWBuffers.RXB1)
        {
            mc_can_getRXBuffer1();
        }
        if (canRequestedOperatingMode != MC_CAN_LEGACY_MODE)
        {

            if (canPendingHWBuffers.RXB2)
            {
                mc_can_getRXBuffer2();
            }
            if (canPendingHWBuffers.RXB3)
            {
                mc_can_getRXBuffer3();
            }
            if (canPendingHWBuffers.RXB4)
            {
                mc_can_getRXBuffer4();
            }
            if (canPendingHWBuffers.RXB5)
            {
                mc_can_getRXBuffer5();
            }
            if (canPendingHWBuffers.RXB6)
            {
                mc_can_getRXBuffer6();
            }
            if (canPendingHWBuffers.RXB7)
            {
                mc_can_getRXBuffer7();
            }
        }
    }
}

void mc_can_getRXBuffer0()
{
    uint8_t addr0_2;
    uint8_t addr3_10;
    
    //disable interrupts while reading the CAN messages so we don't get corrupted data
    //maybe use PIE instead?
    GIE = CAN_REGISTER_CLEAR;
  
    /* Fill the address */
    addr0_2  = RXB0SIDLbits.SID;
    addr3_10 = RXB0SIDH;
    canReceiveBuffer[canReceiveBufferIn].address = CAN_PACK_ADDRESS(addr0_2, addr3_10) ;
    
    /* Fill the data length counter */
    canReceiveBuffer[canReceiveBufferIn].numBytes = RXB0DLCbits.DLC;

    /* Fill the data field */
    canReceiveBuffer[canReceiveBufferIn].data[0] = RXB0D0;
    canReceiveBuffer[canReceiveBufferIn].data[1] = RXB0D1;
    canReceiveBuffer[canReceiveBufferIn].data[2] = RXB0D2;
    canReceiveBuffer[canReceiveBufferIn].data[3] = RXB0D3;
    canReceiveBuffer[canReceiveBufferIn].data[4] = RXB0D4;
    canReceiveBuffer[canReceiveBufferIn].data[5] = RXB0D5;
    canReceiveBuffer[canReceiveBufferIn].data[6] = RXB0D6;
    canReceiveBuffer[canReceiveBufferIn].data[7] = RXB0D7;

    /* Clear Buffer Full Status bit and RXB0IF 
     * MUST HAPPEN or the interrupt will trigger again! (see page 407 of data sheet) 
     */
    RXB0CONbits.RXFUL  = CAN_REGISTER_CLEAR;
    PIR5bits.RXB0IF    = CAN_REGISTER_CLEAR;
    
    //re-enable interrupts now that we are done reading/writing the message from registers
    GIE = CAN_REGISTER_SET;
    
    /* Invalid for received messages, clear it just to be safe */
    canReceiveBuffer[canReceiveBufferIn].priority = 0;
    
    /* Since we just wrote to CAN_receiveBuffer, increment the index */
    can_receiveBufferInIncriment();
}

void mc_can_getRXBuffer1()
{   
    uint8_t addr0_2;
    uint8_t addr3_10;
    //disable interrupts while reading the CAN messages so we don't get corrupted data
    //maybe use PIE instead?
    GIE = CAN_REGISTER_CLEAR;

    /* Fill the address */
    addr0_2  = RXB1SIDLbits.SID;
    addr3_10 = RXB1SIDH;
    canReceiveBuffer[canReceiveBufferIn].address = CAN_PACK_ADDRESS(addr0_2, addr3_10) ;
    
    
    /* Fill the data length counter */
    canReceiveBuffer[canReceiveBufferIn].numBytes = RXB1DLCbits.DLC;

    /* Fill the data field */
    canReceiveBuffer[canReceiveBufferIn].data[0] = RXB1D0;
    canReceiveBuffer[canReceiveBufferIn].data[1] = RXB1D1;
    canReceiveBuffer[canReceiveBufferIn].data[2] = RXB1D2;
    canReceiveBuffer[canReceiveBufferIn].data[3] = RXB1D3;
    canReceiveBuffer[canReceiveBufferIn].data[4] = RXB1D4;
    canReceiveBuffer[canReceiveBufferIn].data[5] = RXB1D5;
    canReceiveBuffer[canReceiveBufferIn].data[6] = RXB1D6;
    canReceiveBuffer[canReceiveBufferIn].data[7] = RXB1D7;

    /* Clear Buffer Full Status bit and RXB0IF 
     * MUST HAPPEN or the interrupt will trigger again! (see page 407 of data sheet) 
     */
    RXB1CONbits.RXFUL  = CAN_REGISTER_CLEAR;
    PIR5bits.RXB1IF    = CAN_REGISTER_CLEAR;
    
    //re-enable interrupts now that we are done reading/writing the message from registers
    GIE = CAN_REGISTER_SET;
    
    /* Invalid for received messages, clear it just to be safe */
    canReceiveBuffer[canReceiveBufferIn].priority = 0;
    
    /* Since we just wrote to CAN_receiveBuffer, increment the index */
    can_receiveBufferInIncriment();
}

/* Todo: Fill these out like mc_can_getRXBuffer0/1 */
void mc_can_getRXBuffer2(){}
void mc_can_getRXBuffer3(){}
void mc_can_getRXBuffer4(){}
void mc_can_getRXBuffer5(){}
void mc_can_getRXBuffer6(){}
void mc_can_getRXBuffer7(){}

void can_receiveBufferInIncriment()
{
    if (canReceiveBufferIn >= CAN_RECEIVE_BUFFER_SIZE)
    {
        canReceiveBufferIn = 0;
    }
    else
    {
        canReceiveBufferIn++;
    }
}

void can_receiveBufferOutIncriment()
{
    if (canReceiveBufferOut >= CAN_RECEIVE_BUFFER_SIZE)
    {
        canReceiveBufferOut = 0;
    }
    else
    {
        canReceiveBufferOut++;
    }
}