/*
 * File:    MC_I2C.h
 * Date:    11/4/16
 */

#ifndef MC_I2C_H
#define MC_I2C_H

#include "EVT_std_includes.h"

/* ===========================================================================================================================
 * Public precompiler definitions
 * ===========================================================================================================================
*/

/* ===========================================================================================================================
 * Public Enumerated type definitions
 * ===========================================================================================================================
 */

/* Set of possible values to use to set the baud of the I2C bus */
typedef enum MC_I2C_BAUD_E
{
    MC_I2C_31KHZ,
    MC_I2C_50KHZ,
    MC_I2C_100KHZ,
    MC_I2C_400KHZ,
    MC_I2C_MAX_BAUD,
    MC_I2C_BAUD_ERROR
} MC_I2C_BAUD_E;

/* ===========================================================================================================================
 * Public Structure definitions
 * ===========================================================================================================================
 */


/* ===========================================================================================================================
 * Public function declarations
 * ===========================================================================================================================
 */

/* Sets the I2C module to its default configuration.
 * 
 * All other MC_I2C functions are undefined until this is called.
 *
 * Undefined:
 *  +   This function has already been called.
 */
void MC_I2C_Init();

/* Sets the baud rate for the next I2C transmission.
 *
 * Parameters:
 *  baud - An MC_I2C_BAUD_E enum value used to indicate the approximate
 *          baud rate desired for the operation of the I2C bus.
 *
 * Return:
 *  +   An enum value indicating the baud rate that the MC is using, or
 *      MC_I2C_BAUD_ERROR if a problem arose during the operation.
 *
 * Undefined:
 *  +   Never
 */
MC_I2C_BAUD_E MC_I2C_SetBaud(MC_I2C_BAUD_E baud);

/* Gets the baud rate for the current I2C transmission if the MC is the active
 * master of the I2C bus, or the baud rate of the next transmission if the MC 
 * is either a slave or inactive master
 *
 * Return:
 *  +   An enum value indicating the baud rate for the I2C module.
 * 
 * Undefined:
 *  +   Never
 */
MC_I2C_BAUD_E MC_I2C_GetBaud();

/* Configures the slave address for the I2C module which will be used when 
 * the MC is not the active master of the bus. Will take affect immediately
 * if the MC is not the active master.
 *
 * Parameters:
 *  addr - The seven-bit number to use as the slave address for the MC when it
 *          is not the master of the bus, or a value less than 1 to disable
 *          slave mode and have the MC exist as an inactive master when it
 *          does not have control of the bus.
 *
 * Undefined:
 *  +   Never
 */
void MC_I2C_SetSlaveAddr(int8_t addr);

/* Gets the current slave address of the I2C module.
 *
 * Returns:
 *  +   The slave address for the I2C module, or -1 if the MC is configured
 *      to return to an inactive master state when not in control of the bus.
 *
 * Undefined:
 *  +   Never
 */
int8_t MC_I2C_GetSlaveAddr();

/* Function for starting an I2C transfer.
 * 
 * Generates a START bit, asserting the MC as the master of the I2C
 * bus. 
 *
 * Undefined:
 *  +   The I2C bus currently has a master.
 */
void MC_I2C_START();

/* Function for initiating an I2C repeated START sequence.
 * 
 * Generates a repeated START condition, indicating that the MC
 * will be retaining control of the I2C lines.
 *
 * Undefined:
 *  +   The MC is not the current master of the bus.
 *  +   A device on the bus is in the process of transmitting data.
 */
void MC_I2C_reSTART();

/* Function for sending an I2C STOP bit and relinquishing
 * control of the bus. Causes the MC to transition from
 * master mode to slave mode or inactive master mode
 *
 * Postconditions:
 *  +   The MC has relinquished ownership of the I2C bus.
 *  +   The MC is in slave mode with a 7-bit slave address.
 *
 * Undefined:
 *  +   The MC is not the master of the bus.  
 *  +   A device on the bus is in the process of transmitting data.
 */
void MC_I2C_STOP();

/* Sends an ACK on the I2C line, indicating that the MC has successfully
 * received a byte of data.
 *
 * Undefined:
 *  +   The MC has not just finished receiving a byte of data from a device
 *      on the I2C bus.
 */
void MC_I2C_ACK();

/* Function that sends a NACK on the I2C line,
 * indicating a failure to receive a byte of
 * data.
 *
 * Undefined:
 *  +   The MC has not just finished receiving a byte of data from a device
 *      on the I2C bus.
 */
void MC_I2C_NACK();

/* Begins the transmission of a single byte of data
 * on the I2C bus.
 * 
 * Parameter:
 * data - The byte to send over the I2C bus.
 *
 * Return:
 *  +   An error code indicating the success of the Tx.
 *      Zero indicates a success. Negative values indicate an error.
 *
 * Undefined:
 *  +   MC_I2C_TxRdy returned false
 *  +   MC_I2C_TxEnable was not called
 */
int8_t MC_I2C_Tx(uint8_t);

/* Reads the most recently received byte from an I2C Rx.
 *
 * Return:
 *  +   The most recently received byte from an I2C Rx.
 *
 * Undefined:
 *  +   MC_I2C_RxRdy returns false
 *  +   MC_I2C_RxEnable was not called
 */
uint8_t MC_I2C_Rx();

/* Determines whether or not the MC is ready to make a call to MC_I2C_Tx()
 *
 * Return:
 *  +   true if the MC is ready for a call to MC_I2C_Tx.
 *
 * Undefined:
 *  +   The ACK from the previous Tx (if there was one) has not been
 *      received.
 *  +   The MC is master of the bus and an Rx has been performed since
 *      the most recent START or repeated START condition.
 *  +   The bus has no master.
 */
bool MC_I2C_TxRdy();

/* Determines whether or not the MC is ready to make a call to MC_I2C_Rx()
 *
 * Return:
 *  +   true if the MC is ready for a call to MC_I2C_Rx.
 *
 * Undefined:
 *  +   MC_I2C_BeginRx was not the last I2C function called.
 */
bool MC_I2C_RxRdy();

/* Checks to see if the MC has completed a Tx that was being performed.
 *
 * Returns:
 *  +   true if the most recent Tx has completed.
 *
 * Undefined:
 *  +   MC_I2C_Tx was not the most recent I2C function called.
 */
bool MC_I2C_TxComplete();

/* Checks to see if the most recent Tx produced an ACK or an NACK.
 *
 * Returns:
 *  +   true if the most recent Tx produced an ACK.
 *
 * Undefined:
 *  +   MC_I2C_TxComplete returned false
 *  +   MC_I2C_IsIdle returned false
 */
bool MC_I2C_ACKReceived();

/* Enables and initiates the reception of a single byte of data.
 *
 * Undefined:
 *  +   When not called immediately after a successful call to ACKReceived.
 */
void MC_I2C_BeginRx();

/* Checks to see if the I2C module is in the process of performing any
 * actions.
 *
 * Returns:
 *  +   true if the I2C module is performing no actions.
 *
 * Undefined:
 *  +   Never
 */
bool MC_I2C_IsIdle();

/* Checks to see if the I2C module is currently the master of the bus
 *
 * Returns:
 *  +   true if the I2C module is the master of the bus
 *
 * Undefined:
 *  +   Never
 */
bool MC_I2C_IsMaster();
#endif
