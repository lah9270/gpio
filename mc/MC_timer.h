/* 
 * File:   MC_timer.h
 * Author: Derek Gutheil <dkg8689@rit.edu>
 *
 * Created on September 9, 2016, 8:07 PM
 */

#ifndef MC_TIMER_H
#define MC_TIMER_H
#include "EVT_std_includes.h"
/* ========================================================================
 * Public Enumerated Types Definitions
 * ========================================================================
 */
typedef enum mc_timer_E {
/* This matches an IO layer ENUM.
 * Do NOT change this without changing both
 */
  MC_TIMER_1,
  MC_TIMER_2,
  MC_TIMER_3,
  MC_TIMER_4,
  NUM_MC_TIMERS
} mc_timer_E;

typedef enum mc_timer_interrupt_periods_E {
/* This matches an IO layer ENUM.
 * Do NOT change this without changing both
 */
  MC_TIMER_100US,
  MC_TIMER_200US,
  MC_TIMER_300US,
  MC_TIMER_400US,
  MC_TIMER_500US,
  MC_TIMER_600US,
  MC_TIMER_700US,
  MC_TIMER_800US,
  MC_TIMER_900US,
  MC_TIMER_1MS,
  MC_TIMER_2MS,
  MC_TIMER_4MS,
  MC_TIMER_8MS,
  MC_TIMER_16MS,   
} mc_timer_interrupt_periods_E;

/* ========================================================================
 * Public Function Declarations
 * ========================================================================
*/

/* Name: 
 *      mc_timer_init
 * Parameters: 
 *      - timer: The enum of the timer to set up
 *      - interrupt_value: The enum of the timer interrupt period desired
 * Description:
 *      Set up a timer. This sets up and enables a timer, and sets the interrupt at the desired frequency 
 *      Default Timer frequencies are 4 micro seconds. 
 *      Modify interrupt frequency via MC_timer_set_interrupt_value
 */
void mc_timer_init(mc_timer_E timer, mc_timer_interrupt_periods_E interrupt_value);

/* Name: 
 *      mc_timer_getStatus
 * Parameters: 
 *      - timer: The enum of the timer to set up
 * Description:
 *      Returns current count/ period of time accumulated in timer
 */
uint8_t mc_timer_getStatus(mc_timer_E timer);
#endif    /* MC_TIMER_H */


