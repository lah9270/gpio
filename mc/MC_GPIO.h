/* 
 * File:   MC_GPIO.h
 * This Module provides access to reading and writing from all GPIO pins on a port
 * Does not yet provide access to the internal pull-up feature
 * 
 * Created on October 13, 2016
 */

#ifndef MC_GPIO_H
#define MC_GPIO_H

#include "EVT_std_includes.h"
#include "MC_pin.h"

/* ===========================================================================================================================
 * Public precompiler definitions
 * ===========================================================================================================================
*/


/* ===========================================================================================================================
 * Public Enumerated type definitions
 * ===========================================================================================================================
*/


/* ===========================================================================================================================
 * Public Structure definitions
 * ===========================================================================================================================
*/


/* ===========================================================================================================================
 * Public function declarations
 * ===========================================================================================================================
*/

/*
 * MC_GPIO_read: Give this function a pin, and it returns the value of that pin
 * Giving an invalid pin will return an error
 * Note: You CAN read from an output pin, doing so will return the value measured at the output of the pin
 *       This is useful for checking to see that the output was properly set, see MC_GPIO_verifyOutputValue()
 * returns: value  : The value of the pin as a MC_PIN_STATES_E type
 *          <0     : Error
 */
MC_PIN_STATES_E MC_GPIO_read(MC_PIN_E pin);

/*
 * MC_GPIO_write: Give this function a pin and a value, and it sets the pin to that value
 * Giving an invalid pin, an improperly configured pin, or an invalid value will return an error
 * returns: 0    : Success
 *          !0   : Error     
 */
int8_t MC_GPIO_write(MC_PIN_E pin, MC_PIN_STATES_E value);

/*
 * MC_GPIO_getOutputValue: Give this function a pin and it returns what the set (written) output of that pin is
 * Giving an invalid pin or an improperly configured pin will return an error
 * Note: This is useful for checking to see that the output was properly set, see MC_GPIO_verifyOutputValue()
 * returns: value  : the value of the desired pin output as a MC_PIN_STATES_E type
 *          <0     :  Error     
 */
MC_PIN_STATES_E MC_GPIO_getOutputValue(MC_PIN_E pin);

/*
 * MC_GPIO_verifyOutputValue: Give this function a pin and it returns whether the set value is the value on the pin
 * This is very useful for checking to that a pin is functioning as expected.
 * This can help detect hardware issues on a board. If a pin is set to be high, but reading it returns low, then
 *      there is most likely a short or high current sink pulling on the board pulling the pin low 
 * returns: 1   : Success, the output of the pin is the desired value
 *          0   : Something is wrong, the output is not what is desired     
 */
uint8_t MC_GPIO_verifyOutputValue(MC_PIN_E pin);

#endif    /* MC_GPIO_H */