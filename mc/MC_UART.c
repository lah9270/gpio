/*
 * File:    MC_UART.c
 * Created: 11/10/2016
 */

#include <stdio.h>
#include "MC_UART.h"
#include "MC_board.h"
#include "MC_processor.h"
#include "MC_register_defaults.h"
#include "usart.h"

/* ========================================================================
 * Private Macro Definitions
 * ========================================================================
*/

/* Define the Pins for the UART channels in order to determine
 * which channel to use. These pins are for the PIC18F25K80 */
#define UART_CHANNEL1_TX_PIN (17)
#define UART_CHANNEL1_RX_PIN (18)

#define UART_CHANNEL2_TX_PIN (27)
#define UART_CHANNEL2_RX_PIN (28)

/* Calculation of Byte in SPBRG Register. 
 * See Section 22.2 of PIC18F datasheet.
*/
/* High Speed Mode (BRGH == 1) */
#define CALC_SPBRG(baud)       ((((FREQUENCY)/(baud))/16)-1)

/* ========================================================================
 * Private Module Variables
 * ========================================================================
*/
/* Store the requested baud and channel to use for the init function */
mc_uart_baud_E uartRequestedBaud;
mc_uart_channel_E uartRequestedChannel;

/* Keep track of the active baud and channel being used */
mc_uart_baud_E uartActiveBaud;
mc_uart_channel_E uartActiveChannel;
/* ========================================================================
 * Module Function Definitions
 * ========================================================================
*/

void putch(unsigned char byte)
{
    while (mc_uart_channelBusy())
    {
        ; /* NULL statement */
    }
    mc_uart_writeData(byte);
}

/* calculates the baud rate for a UART channel. */
uint8_t uart_calcBaudRate(mc_uart_baud_E baud)
{
    uint8_t spbrg;

    switch (baud)
    {
        case MC_UART_BAUD_9600:
            spbrg = CALC_SPBRG(9600);
            break;
        
        case MC_UART_BAUD_19200:
            spbrg = CALC_SPBRG(19200);
            break;
        
        case MC_UART_BAUD_38400:
            spbrg = CALC_SPBRG(38400);
            break;
        
        case MC_UART_BAUD_57600:
            spbrg = CALC_SPBRG(57600);
            break;

        case MC_UART_BAUD_115200:
            spbrg = CALC_SPBRG(115200);
            break;
            
        default:
        {
            /* Invalid option */
        }
    }
    
    return spbrg;
}

/* Opens and configures a UART channel by setting the appropriate bits in
 * the special function registers. Currently supports two channels.
*/
void mc_uart_init()
{
    uint8_t spbrg, config;
    
    /* Determine which UART channel to use */
    if ((MC_BOARD_UART_TX == UART_CHANNEL1_TX_PIN) && (MC_BOARD_UART_RX == UART_CHANNEL1_RX_PIN))
    {
        uartRequestedChannel = MC_UART_CHANNEL1;
    }
    else if ((MC_BOARD_UART_TX == UART_CHANNEL2_TX_PIN) && (MC_BOARD_UART_RX == UART_CHANNEL2_RX_PIN))
    {
        uartRequestedChannel = MC_UART_CHANNEL2;
    }

    /* Determine the configuration for the UART channel */
    config = USART_TX_INT_OFF & USART_RX_INT_OFF & USART_ASYNCH_MODE & USART_EIGHT_BIT & USART_CONT_RX & USART_BRGH_HIGH;
    /* Calculate the value to store in the baud register */    
    spbrg = uart_calcBaudRate(uartRequestedBaud);

    switch (uartRequestedChannel)
    {
        case MC_UART_CHANNEL1:
            Open1USART(config, spbrg);
            break;

        case MC_UART_CHANNEL2:
            Open2USART(config, spbrg);
            break;

        default:
        {
            /* Invalid channel. */
        }
    }

    /* Update the module variables once the init completes */
    uartActiveBaud = uartRequestedBaud;
    uartActiveChannel = uartRequestedChannel;
}

void mc_uart_setBaud(mc_uart_baud_E baud)
{
    uartRequestedBaud = baud;
}

void mc_uart_close()
{
    switch (uartActiveChannel)
    {
        case MC_UART_CHANNEL1:
            Close1USART();
            break;

        case MC_UART_CHANNEL2:
            Close2USART();
            break;

        default:
        {
            /* Invalid channel. */
        }
    }
}
/* Sends a byte of data to the UART peripheral to be transmitted. If the
 * channel is set up for 9-bit mode, it will also set the additional bit.
*/
void mc_uart_writeData(uint8_t data)
{
    switch (uartActiveChannel)
    {
        case MC_UART_CHANNEL1:
            TXREG1 = data;
            break;

        case MC_UART_CHANNEL2:
            TXREG2 = data;
            break;

        default:
        {
            /* Invalid channel. */
        }
    }

}

/* Reads data from the UART channel's hardware data buffer. */
uint8_t mc_uart_readData()
{
    uint8_t data;

    switch (uartActiveChannel)
    {
        case MC_UART_CHANNEL1:
            /* Received Data. */
            data = RCREG1;
            break;

        case MC_UART_CHANNEL2:
            /* Received Data. */
            data = RCREG2;
            break;
       
        default:
        {
            /* Invalid channel. */
        }
    }

    return data; 
}

/* Checks the UART registers to determine if a byte of data has been
 * received.
*/
mc_uart_data_status_E mc_uart_dataReady()
{
    mc_uart_data_status_E retVal;
    switch (uartActiveChannel)
    {
        case MC_UART_CHANNEL1:
            if (PIR1bits.RC1IF)
            {
                if (RCSTA1bits.FERR)
                {
                    /* Frame Error */
                    retVal = MC_UART_FRAME_ERROR;
                }
                else if (RCSTA1bits.OERR)
                {
                    /* Overrun Error */
                    retVal = MC_UART_OVERRUN_ERROR;
                }
                else
                {
                    /* Data is Ready */
                    retVal = MC_UART_DATA_READY;
                }
            }
            else
            {
                /* There is no Ready Data. */
                retVal = MC_UART_NO_DATA;
            }
            break;
        case MC_UART_CHANNEL2:
            if (PIR3bits.RC2IF)
            {
                if (RCSTA2bits.FERR)
                {
                    /* Frame Error */
                    retVal = MC_UART_FRAME_ERROR;
                }
                if (RCSTA2bits.OERR)
                {
                    /* Overrun Error */
                    retVal = MC_UART_OVERRUN_ERROR;
                }
                else
                {
                    /* Data is Ready */
                    retVal = MC_UART_DATA_READY;
                }
            }
            else
            {
                /* There is no Ready Data */
                retVal = MC_UART_NO_DATA;
            }
            break;
        default:
        {
            /* Invalid channel. */
        }
    }
    
    return retVal;
}

/* Checks the UART channel's hardware register to determine if the channel is
 * busy transmitting or is idle.
*/
bool mc_uart_channelBusy()
{
    bool retVal;
    switch (uartActiveChannel)
    {
        case MC_UART_CHANNEL1:
            if (TXSTA1bits.TRMT) /* If Transmit Shift Register is Empty. */
            {
                retVal = false; /* Channel is Idle. */
            }
            else
            {
                retVal = true; /* Channel is Busy. */
            }
            break;

        case MC_UART_CHANNEL2:
            if (TXSTA2bits.TRMT) /* If Transmit Shift Register is Empty. */
            {
                retVal = false; /* Channel is Idle. */
            }
            else
            {
                retVal = true; /* Channel is Busy. */
            }
            break;
            
        default:
        {
            /* Invalid channel. */
        }
    }
    return retVal;
}

/* Checks whether a UART channel is currently open. */
bool mc_uart_isChannelOpen()
{
    bool retVal;
    switch (uartActiveChannel)
    {
        case MC_UART_CHANNEL1:
            if (RCSTA1bits.SPEN)
            {
                retVal = true;
            }
            else
            {
                retVal = false;
            }
            break;
   
        case MC_UART_CHANNEL2:
            if (RCSTA2bits.SPEN)
            {
                retVal = true;
            }
            else
            {
                retVal = false;
            }
            break;

        default:
        {
            /* Invalid channel. */
        }
    }
    return retVal;
}

/* Enables the interrupt trigger when a byte is transmitted on the
 * UART channel specified.
 */
void mc_uart_setTransmitInterrupt(mc_uart_interrupt_status_E status)
{
    switch (uartActiveChannel)
    {
        case MC_UART_CHANNEL1:
            PIE1bits.TX1IE = status;
            break;
            
        case MC_UART_CHANNEL2:
            PIE3bits.TX2IE = status;
            break;
        
        default:
        {
            /* Invalid channel. */
        }
    }
}

/* Checks if the interrupt trigger for a transmission on a given UART channel
 * is set.
 */
mc_uart_interrupt_status_E mc_uart_getTransmitInterrupt()
{
    switch (uartActiveChannel)
    {
        case MC_UART_CHANNEL1:
        {
            return PIE1bits.TX1IE;
        }
        case MC_UART_CHANNEL2:
        {
            return PIE3bits.TX2IE;
        }
        default:
        {
            /* Invalid channel. */
            return 0;
        }
    }
}

/* Enables/disabled the receive trigger when a byte is received on the UART
 * channel specified.
 */
void mc_uart_setReceiveInterrupt(mc_uart_interrupt_status_E status)
{
    switch (uartActiveChannel)
    {
        case MC_UART_CHANNEL1:
        {
            PIE1bits.RC1IE = status;
            break;
        }
        case MC_UART_CHANNEL2:
        {
            PIE3bits.RC2IE = status;
            break;
        }
        default:
        {
            /* Invalid channel. */
        }
    }
}

/* Checks if the interrupt trigger for a received byte on a UART channel is
 * set.
 */
mc_uart_interrupt_status_E mc_uart_getReceiveInterrupt()
{
    switch (uartActiveChannel)
    {
        case MC_UART_CHANNEL1:
        {
            return PIE1bits.RC1IE;
        }
        case MC_UART_CHANNEL2:
        {
            return PIE3bits.RC2IE;
        }
        default:
        {
            /* Invalid channel. */
            return 0;
        }
    }
}

/* Gets the current baud rate for the UART channel. */
mc_uart_baud_E mc_uart_getBaud()
{
    return uartActiveBaud;
}

mc_uart_channel_E mc_uart_getChannel()
{
    return uartActiveChannel;
}
