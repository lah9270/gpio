#ifndef EVT_STD_DEBUG_H
#define EVT_STD_DEBUG_H

#include <stdio.h>

/* Define the debug macros */
#ifdef EVT_DEBUG
/* Checks a condition and prints an error message if it's true */
#define DEBUG_ASSERT(cond, mesg)                      \
    do {                                        \
        if (!(cond))                               \
        {                                       \
            printf("ERROR: %s @ line %d of %s\n\r", mesg, __LINE__, __FILE__); \
        }                                       \
    } while (0)
            
/* Prints an error message if the macro is reached */
#define DEBUG_ERROR(mesg)  printf("ERROR: %s @ line %d of %s\n\r", mesg, __LINE__, __FILE__)

/* Prints an error message if the macro is reached */
#define DEBUG_WARNING(mesg)  printf("WARNING: %s @ line %d of %s\n\r", mesg, __LINE__, __FILE__)

/* Prints a non-error debug message */
#define DEBUG_NOTE(mesg)  printf("NOTE: %s @ line %d of %s\n\r", mesg, __LINE__, __FILE__)

/* Define null macros for when DEBUG is not defined */
#else
#define DEBUG_ASSERT(cond, mesg)    /* NULL MACRO */
#define DEBUG_ERROR(mesg)           /* NULL MACRO */
#define DEBUG_WARNING(mesg)         /* NULL MACRO */
#define DEBUG_NOTE(mesg)            /* NULL MACRO */
#endif

#endif /* ifndef _EVT_STD_DEBUG_H_ */
