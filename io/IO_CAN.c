/* 
 * File:   IO_CAN.c
 * Author: Derek Gutheil <dkg8689@rit.edu>
 *
 * Created on September 8, 2016, 8:07 PM
 */

#include "IO_CAN.h"
#include "MC_CAN.h"
#include <string.h>

void io_can_init()
{
    /* Set up MC CAN parameters */
    mc_can_setRequestedBaudrate(MC_CAN_BAUD_250K);
    mc_can_setRequestedOperatingMode(MC_CAN_LEGACY_MODE);
}

uint8_t io_can_transmitMessage(io_can_message_S * message)
{
    mc_can_frame_S canFrame;
    canFrame.address = message->address;
    canFrame.numBytes = message->numBytes;
    canFrame.priority = 0;
    
    memcpy(canFrame.data, message->data, message->numBytes );
    
    //Send it!
    return mc_can_transmit(&canFrame);
}

void io_can_getMessage(io_can_message_S * returnMessage)
{
    mc_can_frame_S canFrame;
    
    /* Retrieve the message */
    mc_can_getMessage(&canFrame);
    
    /* Move its contents to our return message */
    returnMessage->address = canFrame.address;
    returnMessage->numBytes = canFrame.numBytes;
    memcpy(returnMessage->data, canFrame.data, canFrame.numBytes );
}

uint8_t io_can_bufferIsEmpty()
{
    return mc_can_bufferIsEmpty();
}