/* 
 * File:   IO_timer.c
 * Author: Derek Gutheil <dkg8689@rit.edu>
 *
 * Created on September 9, 2016, 8:07 PM
 */
#include "IO_timer.h"
#include "MC_timer.h"

void io_timer_init(io_timer_E timer, io_timer_interrupt_periods_E interrupt_value){
    mc_timer_init ((mc_timer_E)timer, (mc_timer_interrupt_periods_E)interrupt_value);
}

