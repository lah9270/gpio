/*
 * File: IO_networkManager.h
 * Created: 4/1/2017
 *
 * Responsible for collecting data from other applications and packaging the
 * data into formatted frames for transmission on the CAN bus.
*/

#ifndef IO_NETWORKMANAGER_H
#define IO_NETWORKMANAGER_H

#include <EVT_std_includes.h>

/* ============================================================================
 * Public Enumerated Type Definitions
 * ============================================================================
*/
/* GENERATED MESSAGES ENUM */

/* ============================================================================
 * Public Structure Definitions
 * ============================================================================
*/
/* GENERATED STRUCTS */

/* ============================================================================
 * Public Function Declarations
 * ============================================================================
*/

/* Initializes the network manager IO layer. */
void io_networkManager_init();


/* Instructs the network manager to package a CAN data frame formatted based on
 * a CAN ID with data collected from an application and send it.
 *
 * Arguments:
 *  - id: The CAN message's bus ID.
 *  - data: A pointer to the collected data to package.
 */
void io_networkManager_processFrame(uint16_t id, void * data);

#endif /* IO_NETWORKMANAGER_H */
