/*
 * File:    IO_UART.c
 * Created: 01/10/2017
*/

#include "IO_UART.h"
#include "MC_UART.h"

/* ========================================================================
 * Private Macro Definitions
 * ========================================================================
*/

#define UART_RING_BUFFER_SIZE (126)

/* ========================================================================
 * Private Structure Definitions
 * ========================================================================
*/

/* ========================================================================
 * Private Structure Definitions
 * ========================================================================
*/

typedef struct
{
    uint8_t buffer[UART_RING_BUFFER_SIZE];  /* The buffer to store the data */
    int8_t numElements;                /* Number of elements in the buffer*/
    uint8_t head;                   /* Index of the first element */
    uint8_t tail;                   /* Index of the last element */
} uart_ring_buffer_S;

/* ========================================================================
 * Private Module Variables
 * ========================================================================
*/

io_uart_status_E uartStatus;
io_uart_baud_E uartBaud;

uart_ring_buffer_S uartTXBuffer;
uart_ring_buffer_S uartRXBuffer;

/* ========================================================================
 * Private Function Definitions
 * ========================================================================
*/

/*
 * Enqueues a byte to the back of the ring buffer
 * Returns without modifying the buffer if it's full.
 */
void uart_enqueueByte(uart_ring_buffer_S *rb, uint8_t data)
{
    /* if the buffer is not full, enqueue the data */    
    if (rb->numElements < UART_RING_BUFFER_SIZE)
    {
        rb->buffer[rb->tail] = data;
        if (rb->tail == UART_RING_BUFFER_SIZE - 1)
        {
            /* We've hit the end of the buffer and need to wrap around */
            rb->tail = 0;

        }
        else
        {
            /* Increase the tail location and store the data */
            rb->tail += 1;
        }

        rb->numElements += 1;
    }
}

/*
 * Dequeues and returns a byte from the head of the desired ring buffer
 * If there are no bytes to dequeue, returns 0 without changing the buffer
 */
uint8_t uart_dequeueByte(uart_ring_buffer_S *rb)
{
    uint8_t retData = 0;
    /* If the buffer is not empty, dequeue the data and return it */
    if (0 != rb->numElements)
    {
        retData = rb->buffer[rb->head];
  
        if (rb->head == UART_RING_BUFFER_SIZE - 1)
        {
            /* We've hit the end of the buffer and need to wrap around */
            rb->head = 0;
        }

        else
        {
            /* Increase the head location and retrieve the data */
            rb->head += 1;
        }
        
        rb->numElements -= 1;
    }
    
    return retData;
}

void uart_initRingBuffer(uart_ring_buffer_S *rb)
{
    rb->numElements = 0;
    rb->head = 0;
    rb->tail = 0;
}

/* ========================================================================
 * Module Function Definitions
 * ========================================================================
*/

/* Opens a Serial communication port. */
void io_uart_init()
{
    /* Initialize the module variables */
    uartBaud = IO_UART_BAUD_9600;
    
    /* Set the mc layer's baud */
    mc_uart_setBaud((mc_uart_baud_E) uartBaud);

    /* Don't call mc_uart_init(). The task manager will handle that */
    
    /* Initialize the ring buffers. */
    uart_initRingBuffer(&uartTXBuffer);
    uart_initRingBuffer(&uartRXBuffer);

    uartStatus = IO_UART_GOOD;
}

void io_uart_reopen(io_uart_baud_E baud)
{
    /* close the current channel */
    mc_uart_close();

    /* Set the baud  and reopen uart */
    mc_uart_setBaud((mc_uart_baud_E) baud);
    mc_uart_init();

    /* Update the module variables */
    uartBaud = baud;

    /* Reset the buffers */
    uart_initRingBuffer(&uartTXBuffer);
    uart_initRingBuffer(&uartRXBuffer);

    uartStatus = IO_UART_GOOD;
}

void io_uart_close()
{
    mc_uart_close();
    uartStatus = IO_UART_OFF;
}

/* Handles the UART operations */
void io_uart_process()
{
    mc_uart_data_status_E rxStat;

    /* See if the selected channel is open */
    if (true == mc_uart_isChannelOpen())
    {
        /*----------Transmit----------*/

        /* See if there is data to transmit */
        if (0 != uartTXBuffer.numElements)
        {
            /* See if we're free to send data out */
            if (!mc_uart_channelBusy())
            {
                 /* If so, dequeue a byte and send it out */
                mc_uart_writeData(uart_dequeueByte(&uartTXBuffer));
            }
        }

        /*----------Receive----------*/

        /* See if there is data to receive */
        rxStat = mc_uart_dataReady();

        /* If there is data and the buffer isn't full, load store it in the RXBuffer */
        if (MC_UART_DATA_READY == rxStat)
        {
            if (UART_RING_BUFFER_SIZE != uartRXBuffer.numElements)
            {
                uart_enqueueByte(&uartRXBuffer, mc_uart_readData());
            }
            
            else
            {
                uartStatus = IO_UART_DATA_LOST;
            }
            
        }

        /* See if there was an overrun error in the UART module */
        else if (MC_UART_OVERRUN_ERROR == rxStat)
        {
            uartStatus = IO_UART_DATA_LOST;
        }

        /* See if there was a frame error in the UART module */
        else if (MC_UART_FRAME_ERROR == rxStat)
        {
            uartStatus = IO_UART_FRAME_ERROR;
        }
    }
}

uint8_t io_uart_readN(uint8_t *data, uint8_t num)
{
    uint8_t i = 0;
    
    /* while there is data to read and we haven't read 'num' bytes */
    while ((0 != uartRXBuffer.numElements) && (i < num))
    {
        data[i] = uart_dequeueByte(&uartRXBuffer);
        ++i;
    }
   
    /* returns the number of bytes read */
    return i;
}

uint8_t io_uart_writeN(uint8_t *data, uint8_t num)
{
    uint8_t i = 0;

    /* While there is data to write and our buffer isn't full */
    while ((UART_RING_BUFFER_SIZE > uartTXBuffer.numElements) && (i < num))
    {
        uart_enqueueByte(&uartTXBuffer, data[i]);
        ++i;
    }

    /* returns the number of bytes written */
    return i;
}

/* Writes an hexadecimal number to the Serial port as a string. */
int8_t io_uart_printHex(uint16_t data)
{

    int8_t retData;             /* Data to return */
    uint8_t i;                  /* Index variable */
    uint8_t printLen;           /* The number of chars to print */
    uint8_t digit;              /* Temporary variable for a single digit */
    char temp[] = "0x0000";  /* The buffer to hold the resulting string */
    printLen = (sizeof(temp)/sizeof(char));

    /* Default to return -1, indicating an error */
    retData = -1;
    
    /* If we have enough space in the buffer, print the number */
    if (UART_RING_BUFFER_SIZE - printLen >= uartTXBuffer.numElements)
    {
        /* It's a 16-bit number so we'll need four digits */
        for (i=0; i != 4; ++i)
        {
            /* Isolate the nibble */
            digit = (data >> (i*4)) & 0x0F;

            /* see if we need a number or a letter for the digit */
            if (digit < 10)
            {
                digit += '0';
            }

            else
            {
                digit -= 10;
                digit += 'A';
            }

            /* Assign the temp array the proper characters, starting with
             * the first non NULL character. */
            temp[(printLen-2)-i] = digit;
        }

        /* Print the data to the buffer and make sure we didn't overflow the buffer */
        if (printLen != io_uart_writeN((uint8_t*)temp, printLen))
        {
            uartStatus = IO_UART_DATA_LOST;
        }
        else
        {
            /* Print was successful, so return 0 */
            retData = 0;
        }
    }

    return retData;
}

/* Determines whether data is being held in the serial port buffer or if an
 * error has occurred in the transmission. */
io_uart_status_E io_uart_getStatus()
{
    return uartStatus;
}
