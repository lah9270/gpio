/* 
 * File:   IO_LTC6802.h
 *
 * Created on March 31, 2016, 10:07 PM
 */

#ifndef IO_LTC6802_H
#define IO_LTC6802_H

#include "EVT_std_includes.h"

/* ===========================================================================================================================
 * Public precompiler definitions
 * ===========================================================================================================================
*/

/* Used for allocating space for cell voltage readings  */
#define IO_LTC6802_MAX_NUM_BOARDS  (4)
#define IO_LTC6802_BYTES_PER_BOARD (24) /* (2 byte voltage readings * 12 readings per board) */


/* ===========================================================================================================================
 * Public Enumerated type definitions
 * ===========================================================================================================================
 */
 
typedef enum io_ltc6802_discharge_cell_E
{
    IO_LTC6802_DISCHARGE_CELL_DEFAULT    = 0u,
    IO_LTC6802_DISCHARGE_CELL_OFF        = 0u, /* Turn off discharge switch for cell */
    IO_LTC6802_DISCHARGE_CELL_ON         = 1u, /* Turn on discharge switch for cell  */
} io_ltc6802_discharge_cell_E;

typedef enum io_ltc6802_tos_E
{
    IO_LTC6802_BOTTOM_OF_STACK  = 0u,   /* Indicates that board is on the bottom of the stack */
    IO_LTC6802_MIDDLE_OF_STACK  = 0u,   /* Indicates that board is anywhere in the middle */
    IO_LTC6802_TOP_OF_STACK     = 1u,   /* Indicates that board is on the top of the stack */
    
} io_ltc6802_tos_E;

typedef enum io_ltc_vmode_E
{
    IO_LTC6802_VMODE_LOW        = 0u,   /* Current driven communication for non-bottom of stack boards*/
    IO_LTC6802_VMODE_HIGH       = 1u,   /* Voltage driven communication for bottom of stack board */
} io_ltc6802_vmode_E;

/* ===========================================================================================================================
 * Public Structure definitions
 * ===========================================================================================================================
 */

 
/* CELL VOLTAGES */
typedef union {
    struct {
        uint16_t value;
    };
    struct {
        uint8_t lowBits;
        uint8_t highBits;
    };
} io_ltc6802_cellV;



/* ===========================================================================================================================
 * Public function declarations
 * ===========================================================================================================================
 */

/***********************************************************************************
* Name        : io_ltc6802_init
* Description : Initializes local LTC module variables, writes a starting config
*               to the config register, and sets board stack parameters. 
*               CALLED BY THE APP LAYER AFTER DETERMINING NUMBER OF BOARDS IN STACK.
* Pre         : None
* @param      : numBoards - Number of boards in the stack, determined by app layer
*               boardPos - Position of board in the stack, 0 is the bottom
* @return     : None
***********************************************************************************/
void io_ltc6802_init(uint8_t numBoards, uint8_t boardPos);

/***********************************************************************************
* Name        : io_ltc6802_updateDischargeStatus
* Description : Writes a new congifuration to begin discharging the specified battery cells
* Pre         : LTC is initialized
* @param      : dcc - Array of voltage statuses, 
*                     indexed at 0 for board 0 cell 1, board 0 cell 2...board X cell 12
* @return     : None
***********************************************************************************/
void io_ltc6802_updateDischargeStatus(io_ltc6802_discharge_cell_E *dcc);

/***********************************************************************************
* Name        : io_ltc6802_getCellVoltages
* Description : Gets the current voltage value for each cell
* Pre         : LTC is initialized
* @param      : None
* @return     : ltc6802AllCellVoltages - Array of cell voltages, 
*                                        indexed at 0 for board 0 cell 1, board 0 cell 2...board X cell 12
***********************************************************************************/
io_ltc6802_cellV* io_ltc6802_getCellVoltages();

/***********************************************************************************
* Name        : io_ltc6802_stateMachine
* Description : State machine for A/D voltage conversions and gathering cell voltage
*               data. Called every 10ms in task manager.
* Pre         : LTC is initialized
* @param      : None
* @return     : None
***********************************************************************************/
void io_ltc6802_stateMachine();

/***********************************************************************************
* Name        : io_ltc6802_testLTC
* Description : Runs through writing and reading back to the config register, and prints
*               any errors over UART.
* Pre         : LTC is initialized
* @param      : Number of boards in the stack
* @return     : None
***********************************************************************************/
void io_ltc6802_testLTC(uint8_t numBoards);

#endif    /* IO_LTC6802_H */

