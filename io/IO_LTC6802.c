/* 
 * File:   IO_LTC6802.c
 *
 * Created on March 31, 2016, 10:07 PM
 */
#include <stdio.h>
#include "IO_LTC6802.h"
#include "IO_SPI.h"
#include "MC_board.h"
#include "EVT_std_debug.h"

/* ========================================================================
 * Private Macro Definitions
 * ========================================================================
*/

/* Command Codes (See Table 6 page 23 of the datasheet) ====================================*/
/* Write Configuration Register Group */
#define LTC6802_CMD_WRITE_CONFIG              (0x01)
/* Read Configuration Register Group */
#define LTC6802_CMD_READ_CONFIG               (0x02)
/* Read Cell Voltage Register Group */
#define LTC6802_CMD_READ_CELL_VOLTAGES        (0x04)
/* Read Flag Register Group */
#define LTC6802_CMD_READ_FLAGS                (0x06)
/* Read Temperature Register Group */
#define LTC6802_CMD_READ_TEMPS                (0x08)
/* Start Cell Voltage A/D Conversions and Poll Status */
#define LTC6802_CMD_START_CELL_VOLTAGE_AD     (0x10)
/* Start Open Wire A/D Conversions and Poll Status */
#define LTC6802_CMD_START_OPEN_WIRE_AD        (0x20)
/* Start Temperature A/D Conversions and Poll Status */
#define LTC6802_CMD_START_TEMP_AD             (0x30)
/* Poll A/D Converter Status */
#define LTC6802_CMD_POLL_AD_STATUS            (0x40)
/* Poll Interrupt Status */
#define LTC6802_CMD_POLL_INTERRUPT_STATUS     (0x50)
/* Start Cell Voltage A/D Conversions and Poll Status, with Discharge Permitted */
#define LTC6802_CMD_START_CELL_VOLTAGE_AD_DC  (0x60)
/* Start Open Wire A/D Conversions and Poll Status, with Discharge Permitted */
#define LTC6802_CMD_START_OPEN_WIRE_AD_DC     (0x70)
/* =========================================================================================*/


/* === Array Sizes === */
#define CONFIG_ARR_SIZE   (6)
#define VOLTAGE_ARR_SIZE  (12)
#define VOLTAGE_REG_SIZE  (18)
#define PEC_REG_SIZE      (1)

/* Wait constant for x amount of task manager calls before new cell readings are taken */
#define WAITING_TM_ITERATIONS  (10)

/* Used for updating discharge status */
#define NUM_CELLS (12)

/* Constants to interpret raw cell data */
#define LTC6802_VOLTAGE_BIT_MASK   (0x0FFF)
#define LTC6802_VOLTAGE_BIT_SHIFT  (4)
#define LTC6802_MILLIVOLTS_PER_BIT (1.5)

/* Under- and over-voltage comparison numbers.
 * Calculation as per data sheet:
 *        VOV / (16 * 1.5mV)
 *        VUV / (16 * 1.5mV)
 *
 * VUV = 3048mV - Desired undervoltage value for our battery cells
 * VOV = 4200mV -    "    overvoltage    "    "    "    "    "
 */
#define UNDER_VOLTAGE (127)
#define OVER_VOLTAGE  (175)

/* ===========================================================================================================================
 * Private Enumerated type definitions
 * ===========================================================================================================================
 */
typedef enum ltc6802_comparator_duty_cycle_E
{
    LTC6802_CDC_DEFAULT      = 0u,

    /* UV/OV Comparator Period                = NA Standby mode
     * Vref Powered Down Between Measurements = Yes
     * Cell Voltage Measurement Time          = NA */
    LTC6802_CDC_OFF_STANDBY  = 0u,

    /* UV/OV Comparator Period                = NA
     * Vref Powered Down Between Measurements = No
     * Cell Voltage Measurement Time          = 13ms */
    LTC6802_CDC_OFF          = 1u,

    /* UV/OV Comparator Period                = 13ms
     * Vref Powered Down Between Measurements = No
     * Cell Voltage Measurement Time          = 13ms */
    LTC6802_CDC_13MS         = 2u,

    /* UV/OV Comparator Period                = 130ms
     * Vref Powered Down Between Measurements = No
     * Cell Voltage Measurement Time          = 13ms */
    LTC6802_CDC_130MS        = 3u,

    /* UV/OV Comparator Period                = 500ms
     * Vref Powered Down Between Measurements = No
     * Cell Voltage Measurement Time          = 13ms */
    LTC6802_CDC_500MS        = 4u,

    /* UV/OV Comparator Period                = 130ms
     * Vref Powered Down Between Measurements = Yes
     * Cell Voltage Measurement Time          = 21ms */
    LTC6802_CDC_130MS_PD     = 5u,

    /* UV/OV Comparator Period                = 500ms
     * Vref Powered Down Between Measurements = Yes
     * Cell Voltage Measurement Time          = 21ms */
    LTC6802_CDC_500MS_PD     = 6u,

    /* UV/OV Comparator Period                = 2000ms
     * Vref Powered Down Between Measurements = Yes
     * Cell Voltage Measurement Time          = 21ms */
    LTC6802_CDC_2000MS_PD    = 7u,
} ltc6802_comparator_duty_cycle_E;

typedef enum ltc6802_cell10_mode_E
{
    LTC6802_CELL_10_MODE_DEFAULT   = 0u,
    LTC6802_12_CELL_MODE           = 0u, /* 12 cell mode */
    LTC6802_10_CELL_MODE           = 1u, /* 10 cell mode */
} ltc6802_cell10_mode_E;

typedef enum ltc6802_level_poll_mode_E
{
    LTC6802_LEVEL_POLL_MODE_DEFAULT   = 0u,
    LTC6802_TOGGLE_POLL_MODE          = 0u, /* Toggle Polling */
    LTC6802_LEVEL_POLL_MODE           = 1u, /* Level Polling  */
} ltc6802_level_poll_mode_E;

typedef enum ltc6802_GPIO_E
{
    LTC6802_GPIO_DEFAULT         = 1u,
    LTC6802_GPIO_PULL_DOWN_ON    = 0u, /* (In write mode) => GPIO pin pull down on  */
    LTC6802_GPIO_LOW             = 0u, /* (In read mode)  => GPIO pin at logic '0'  */
    LTC6802_GPIO_PULL_DOWN_OFF   = 1u, /* (In write mode) => GPIO pin pull down off */
    LTC6802_GPIO_HIGH            = 1u, /* (In read mode)  => GPIO pin at logic '1'  */
} ltc6802_GPIO_E;

typedef enum ltc6802_watchdog_timer_E
{
    LTC6802_WATCHDDOG_LOW        = 0u, /* (read only) WDTB pin at logic '0' */
    LTC6802_WATCHDDOG_HIGH       = 1u, /* (read only) WDTB pin at logic '1' */
} ltc6802_watchdog_timer_E;

typedef enum ltc6802_discharge_cell_E
{
    LTC6802_DISCHARGE_CELL_DEFAULT    = 0u,
    LTC6802_DISCHARGE_CELL_OFF        = 0u, /* Turn off discharge switch for cell */
    LTC6802_DISCHARGE_CELL_ON         = 1u, /* Turn on discharge switch for cell  */
} ltc6802_discharge_cell_E;

typedef enum ltc6802_undervolt_compare_E
{
    LTC6802_UNDERVOLT_COMPARE_DEFAULT = 0u,
    /* For the future we may use (VUV * 16* 1.5mV)
    for undervoltage comparison, using CDC duty cycles
    that are not 0 or 1*/
} ltc6802_undervolt_compare_E;

typedef enum ltc6802_overvolt_compare_E
{
    LTC6802_OVERVOLT_COMPARE_DEFAULT = 0u,
    /* For the future we may use (VOV * 16* 1.5mV)
    for undervoltage comparison, using CDC duty cycles
    that are not 0 or 1*/
} ltc6802_overvolt_compare_E;

typedef enum ltc6802_cell_interrupt_mask_E
{
    LTC6802_CELL_INTERRUPT_DEFAULT     = 0u,
    LTC6802_CELL_INTERRUPT_ENABLE    = 0u,  /* Enable cell interrupts for cell */
    LTC6802_CELL_INTERRUPT_CLEAR    = 1u   /* Clear cell interrupt flag AND TURNS
                                            * OFF CELL INTERRUPTS */
} ltc6802_cell_interrupt_mask_E;

typedef enum ltc6802_cell_undervolt_flag_E
{
    LTC6802_CELL_UNDERVOLT_NOT_FLAGGED  = 0u, /* Cell not in undervolt state */
    LTC6802_CELL_UNDERVOLT_FLAGGED      = 1u  /* Cell in undervolt state */
} ltc6802_cell_undervolt_flag_E;

typedef enum ltc6802_cell_overvolt_flag_E
{
    LTC6802_CELL_OVERVOLT_NOT_FLAGGED  = 0u, /* Cell not in overvolt state */
    LTC6802_CELL_OVERVOLT_FLAGGED      = 1u  /* Cell in overvolt state */
} ltc6802_cell_overvolt_flag_E;

/* Enum for state machine */
typedef enum ltc6802_state_E
{
    LTC6802_INIT,
    LTC6802_START_AD_CONVERSION,
    LTC6802_CHECK_AD_FINISHED,
    LTC6802_READ_VOLTAGES,
    LTC6802_CONVERT_VOLTAGES,
    LTC6802_WAIT
} ltc6802_state_E;

/* ===========================================================================================================================
 * Private Structure definitions
 * ===========================================================================================================================
 */
 typedef struct ltc6802_config_reg_group_S
{
    union 
    {
        struct 
        {
            uint8_t CFGR0;
        };
        struct 
        {
            uint8_t cdc    :3; /* For some reason it won't build if "cdc" is all caps...? */
            uint8_t CELL10 :1;
            uint8_t LVLPL  :1;
            uint8_t GPIO1  :1;
            uint8_t GPIO2  :1;
            uint8_t WDT    :1;
        };
    };

    union 
    {
        struct 
        {
            uint8_t CFGR1;
        };
        struct 
        {
            uint8_t DCC1 :1;
            uint8_t DCC2 :1;
            uint8_t DCC3 :1;
            uint8_t DCC4 :1;
            uint8_t DCC5 :1;
            uint8_t DCC6 :1;
            uint8_t DCC7 :1;
            uint8_t DCC8 :1;
        };
    };

    union
    {
        struct
        {
            uint8_t CFGR2;
        };
        struct
        {
            uint8_t DCC9 :1;
            uint8_t DCC10:1;
            uint8_t DCC11:1;
            uint8_t DCC12:1;
            uint8_t MC1I :1;
            uint8_t MC2I :1;
            uint8_t MC3I :1;
            uint8_t MC4I :1;
        };
    };
    
    union
    {
        struct
        {
            uint8_t CFGR3;
        };
        struct
        {
            uint8_t MC5I :1;
            uint8_t MC6I :1;
            uint8_t MC7I :1;
            uint8_t MC8I :1;
            uint8_t MC9I :1;
            uint8_t MC10I:1;
            uint8_t MC11I:1;
            uint8_t MC12I:1;
        };
    };
    
    union
    {
        struct
        {
            uint8_t CFGR4;
        };
        struct
        {
            uint8_t VUV0 :1;
            uint8_t VUV1 :1;
            uint8_t VUV2 :1;
            uint8_t VUV3 :1;
            uint8_t VUV4 :1;
            uint8_t VUV5 :1;
            uint8_t VUV6 :1;
            uint8_t VUV7 :1;
        };
    };
    
    union
    {
        struct
        {
            uint8_t CFGR5;
        };
        struct
        {
            uint8_t VOV0 :1;
            uint8_t VOV1 :1;
            uint8_t VOV2 :1;
            uint8_t VOV3 :1;
            uint8_t VOV4 :1;
            uint8_t VOV5 :1;
            uint8_t VOV6 :1;
            uint8_t VOV7 :1;
        };
    };
    
} ltc6802_config_reg_group_S;


/* ===========================================================================================================================
 * Private module variables
 * ===========================================================================================================================
*/
static uint8_t ltc6802TotalBoards = 0;
static ltc6802_config_reg_group_S ltc6802Config[IO_LTC6802_MAX_NUM_BOARDS];
static ltc6802_state_E ltc6802State = LTC6802_INIT;
static io_spi_sync_modes_E ltc6802SpiSync;
static io_spi_bus_modes_E ltc6802SpiBus;
static io_spi_sample_modes_E ltc6802SpiSample;
io_ltc6802_cellV ltc6802AllCellVoltages[(IO_LTC6802_MAX_NUM_BOARDS * IO_LTC6802_BYTES_PER_BOARD)];

/* ===========================================================================================================================
 * Private module function declarations
 * ===========================================================================================================================
*/
static void ltc6802_writeConfig(ltc6802_config_reg_group_S *configReg);
static void ltc6802_readConfig(ltc6802_config_reg_group_S *configRead);
static void ltc6802_storeVoltages(uint8_t *cellVoltageReg, io_ltc6802_cellV *cellVoltages);
static void ltc6802_interpretVoltages(io_ltc6802_cellV *cellVoltages);
static void ltc6802_startVoltageADConvert();
static void ltc6802_setVMODE(io_ltc6802_vmode_E vmode);
static void ltc6802_setTOS(io_ltc6802_tos_E tos);

/* ===========================================================================================================================
 *  Module function definitions
 * ===========================================================================================================================
*/

void io_ltc6802_init(uint8_t numBoards, uint8_t boardPos)
{
    int boardNum;
    
    ltc6802TotalBoards = numBoards;
    
    /* Set top of stack and VMODE for each board*/
    if((boardPos == (numBoards - 1)) && (numBoards >= 2)) /* TOP BOARD */
    {
        ltc6802_setTOS(IO_LTC6802_TOP_OF_STACK);
        ltc6802_setVMODE(IO_LTC6802_VMODE_LOW);
    }
    else if((boardPos > 0) && (numBoards >= 3)) /* MIDDLE BOARDS */
    {
        ltc6802_setTOS(IO_LTC6802_MIDDLE_OF_STACK);
        ltc6802_setVMODE(IO_LTC6802_VMODE_LOW);
    }
    else /* BOTTOM BOARD */
    {
        if(numBoards == 1)
        {
            ltc6802_setTOS(IO_LTC6802_TOP_OF_STACK);
            ltc6802_setVMODE(IO_LTC6802_VMODE_HIGH);
        }
        else
        {
            ltc6802_setTOS(IO_LTC6802_BOTTOM_OF_STACK);
            ltc6802_setVMODE(IO_LTC6802_VMODE_HIGH);
        }
        
        ltc6802SpiSync = io_spi_getRequestedSyncMode();
        ltc6802SpiBus = io_spi_getRequestedBusMode();
        ltc6802SpiSample = io_spi_getRequestedSampleMode();
        io_spi_openCS(MC_BOARD_CS_P);
        
        /* Populate default configuration registers and set stack parameters */
        /* INDEX 0 IS THE BOTTOM BOARD */
        for(boardNum = 0; boardNum < ltc6802TotalBoards; boardNum++)
        {
            
            /* Configuration Register 0 */
            ltc6802Config[boardNum].cdc    = LTC6802_CDC_500MS;
            ltc6802Config[boardNum].CELL10 = LTC6802_12_CELL_MODE;
            ltc6802Config[boardNum].LVLPL  = LTC6802_LEVEL_POLL_MODE;
            ltc6802Config[boardNum].GPIO1  = LTC6802_GPIO_PULL_DOWN_ON;
            ltc6802Config[boardNum].GPIO2  = LTC6802_GPIO_PULL_DOWN_ON;
            
            /* Register 1 */
            ltc6802Config[boardNum].DCC1 = LTC6802_DISCHARGE_CELL_DEFAULT;
            ltc6802Config[boardNum].DCC2 = LTC6802_DISCHARGE_CELL_DEFAULT;
            ltc6802Config[boardNum].DCC3 = LTC6802_DISCHARGE_CELL_DEFAULT;
            ltc6802Config[boardNum].DCC4 = LTC6802_DISCHARGE_CELL_DEFAULT;
            ltc6802Config[boardNum].DCC5 = LTC6802_DISCHARGE_CELL_DEFAULT;
            ltc6802Config[boardNum].DCC6 = LTC6802_DISCHARGE_CELL_DEFAULT;
            ltc6802Config[boardNum].DCC7 = LTC6802_DISCHARGE_CELL_DEFAULT;
            ltc6802Config[boardNum].DCC8 = LTC6802_DISCHARGE_CELL_DEFAULT;
            
            /* Register 2 */
            ltc6802Config[boardNum].DCC9  = LTC6802_DISCHARGE_CELL_DEFAULT;
            ltc6802Config[boardNum].DCC10 = LTC6802_DISCHARGE_CELL_DEFAULT;
            ltc6802Config[boardNum].DCC11 = LTC6802_DISCHARGE_CELL_DEFAULT;
            ltc6802Config[boardNum].DCC12 = LTC6802_DISCHARGE_CELL_DEFAULT;
            ltc6802Config[boardNum].MC1I  = LTC6802_CELL_INTERRUPT_DEFAULT;
            ltc6802Config[boardNum].MC2I  = LTC6802_CELL_INTERRUPT_DEFAULT;
            ltc6802Config[boardNum].MC3I  = LTC6802_CELL_INTERRUPT_DEFAULT;
            ltc6802Config[boardNum].MC4I  = LTC6802_CELL_INTERRUPT_DEFAULT;
            
            /* Register 3 */
            ltc6802Config[boardNum].MC5I  = LTC6802_CELL_INTERRUPT_DEFAULT;
            ltc6802Config[boardNum].MC6I  = LTC6802_CELL_INTERRUPT_DEFAULT;
            ltc6802Config[boardNum].MC7I  = LTC6802_CELL_INTERRUPT_DEFAULT;
            ltc6802Config[boardNum].MC8I  = LTC6802_CELL_INTERRUPT_DEFAULT;
            ltc6802Config[boardNum].MC9I  = LTC6802_CELL_INTERRUPT_DEFAULT;
            ltc6802Config[boardNum].MC10I = LTC6802_CELL_INTERRUPT_DEFAULT;
            ltc6802Config[boardNum].MC11I = LTC6802_CELL_INTERRUPT_DEFAULT;
            ltc6802Config[boardNum].MC12I = LTC6802_CELL_INTERRUPT_DEFAULT;
            
            /* Register 4 */
            ltc6802Config[boardNum].CFGR4 = UNDER_VOLTAGE;
            
            /* Register 5 */
            ltc6802Config[boardNum].CFGR5 = OVER_VOLTAGE;
        } /* for()...*/
    } /* else */
}

/* Sets the top of stack pin for the board. TOS pin high for top of the stack only*/
static void ltc6802_setTOS(io_ltc6802_tos_E tos)
{
    MC_pin_setDirection(MC_BOARD_TOS, MC_PIN_OUTPUT);
    
    if(tos == IO_LTC6802_TOP_OF_STACK)
    {
        MC_GPIO_write(MC_BOARD_TOS, MC_PIN_HIGH);
    }
    else
    {
        MC_GPIO_write(MC_BOARD_TOS, MC_PIN_LOW);
    }
    
}

/* Sets the VMODE pin for a single board. Low for bottom of stack only.*/
static void ltc6802_setVMODE(io_ltc6802_vmode_E vmode)
{
    MC_pin_setDirection(MC_BOARD_VMODE, MC_PIN_OUTPUT);
    
    if(vmode == IO_LTC6802_VMODE_LOW)
    {
        MC_GPIO_write(MC_BOARD_VMODE, MC_PIN_LOW);
    }
    else
    {
        MC_GPIO_write(MC_BOARD_VMODE, MC_PIN_HIGH);
    }
}

/* Writes a new configuration for the LTCs on each board.
 * ASSUMES INDEX 0 IS THE BOTTOM BOARD.
 * IT IS REQUIRED TO WRITE CONFIGS IN ORDER FROM TOP TO BOTTOM */
static void ltc6802_writeConfig(ltc6802_config_reg_group_S *configReg)
{
    uint8_t configArray[CONFIG_ARR_SIZE * IO_LTC6802_MAX_NUM_BOARDS];
    uint8_t command[1];
    int8_t i = 0;
    int8_t j = 0;
    command[0] = LTC6802_CMD_WRITE_CONFIG;
    
    io_spi_toggleCSLow(MC_BOARD_CS_P);
    io_spi_write(command, 1);
    
    /* Populate configuration array with new desired config for each board */
    for(i = ltc6802TotalBoards-1; i >= 0; i--)
    {
        configArray[j+0] = configReg[i].CFGR0;
        configArray[j+1] = configReg[i].CFGR1;
        configArray[j+2] = configReg[i].CFGR2;
        configArray[j+3] = configReg[i].CFGR3;
        configArray[j+4] = configReg[i].CFGR4;
        configArray[j+5] = configReg[i].CFGR5;
        
        j += CONFIG_ARR_SIZE;
    }
    
    /* Send the data to the config registers (TOP -> MID -> BOTTOM) */
    io_spi_write(configArray, (CONFIG_ARR_SIZE * ltc6802TotalBoards));
    io_spi_toggleCSHigh(MC_BOARD_CS_P);
    
}

/* Reads the configuration register from the LTC
 * ASSUMES INDEX 0 IS THE BOTTOM, SAME AS ABOVE
 * IT IS REQUIRED TO READ CONFIGS IN ORDER FROM BOTTOM TO TOP */
static void ltc6802_readConfig(ltc6802_config_reg_group_S *configRead)
{
    uint8_t i;
    uint8_t configArray[(CONFIG_ARR_SIZE + PEC_REG_SIZE)* IO_LTC6802_MAX_NUM_BOARDS];
    uint8_t command[1];
    uint8_t boardNum = 0;
    uint8_t j = 0;
    command[0] = LTC6802_CMD_READ_CONFIG;
    
    
    /* Read the data from the config registers (BOTTOM -> MID -> TOP) */
    io_spi_toggleCSLow(MC_BOARD_CS_P);
    io_spi_write(command, 1);
    
    io_spi_read(configArray, ((CONFIG_ARR_SIZE + PEC_REG_SIZE) * ltc6802TotalBoards));
    
    /* Populate configuration with newly-aquired config data */
    for(i = 0; i < ltc6802TotalBoards; i++)
    {
        /* Boards are being read from bottom to top */
        configRead[i].CFGR0 = configArray[j+0];
        configRead[i].CFGR1 = configArray[j+1];
        configRead[i].CFGR2 = configArray[j+2];
        configRead[i].CFGR3 = configArray[j+3];
        configRead[i].CFGR4 = configArray[j+4];
        configRead[i].CFGR5 = configArray[j+5];
        
        j+= (CONFIG_ARR_SIZE + PEC_REG_SIZE);
    }
    
    io_spi_toggleCSHigh(MC_BOARD_CS_P);
}

/* Used to read cell voltages on each iteration of task manager */
void io_ltc6802_stateMachine()
{
    uint8_t i;
    uint8_t command[1];
    static uint8_t cellVoltageReg[(VOLTAGE_REG_SIZE + PEC_REG_SIZE)*IO_LTC6802_MAX_NUM_BOARDS];
    static int8_t stateIterations;
    
    switch(ltc6802State)
    {
        case LTC6802_INIT:
            
            stateIterations = WAITING_TM_ITERATIONS;
            /* Check if init was called by app */
            if(ltc6802TotalBoards != 0)
            {
                ltc6802_writeConfig(ltc6802Config);
                ltc6802State = LTC6802_START_AD_CONVERSION;
            }
            else
            {
                ltc6802State = LTC6802_INIT;
            }
            
            break;
            
        case LTC6802_START_AD_CONVERSION:
            
            ltc6802_startVoltageADConvert();
            
            ltc6802State = LTC6802_CHECK_AD_FINISHED;
            break;
            
        case LTC6802_CHECK_AD_FINISHED:
            /* Send a polling command to the LTC to check for AD completion*/
            command[0] = LTC6802_CMD_POLL_AD_STATUS;
            io_spi_toggleCSLow(MC_BOARD_CS_P);
            io_spi_write(command, 1);
            
            /* Change SPI input to GPIO to check if AD conversions are done */
            io_spi_close();
            MC_pin_setDirection(MC_BOARD_SDI_P, MC_PIN_INPUT);
            
            /* If LTC pulls pin high, AD conversions are done */
            if(MC_GPIO_read(MC_BOARD_SDI_P) == MC_PIN_HIGH)
            {   
                ltc6802State = LTC6802_READ_VOLTAGES;
            }
            else
            {
                ltc6802State = LTC6802_CHECK_AD_FINISHED;
            }
            /* Re-initialize SPI channel with previously used settings */
            /* so everyone else is free to use SPI */
            io_spi_reopen(ltc6802SpiSync, ltc6802SpiBus, ltc6802SpiSample);
            io_spi_toggleCSHigh(MC_BOARD_CS_P);
            break;
            
        case LTC6802_READ_VOLTAGES:
            
            /* Read all of the cell voltages */
            command[0] = LTC6802_CMD_READ_CELL_VOLTAGES;
            io_spi_toggleCSLow(MC_BOARD_CS_P);
            io_spi_write(command, 1);
            
            io_spi_read(cellVoltageReg, (VOLTAGE_REG_SIZE + PEC_REG_SIZE)*ltc6802TotalBoards);
            io_spi_toggleCSHigh(MC_BOARD_CS_P);
            
            ltc6802State = LTC6802_CONVERT_VOLTAGES;
            
        case LTC6802_CONVERT_VOLTAGES:
        
            /* Store the voltages we just read in */
            for(i=0; i < ltc6802TotalBoards; i++)
            {
                ltc6802_storeVoltages(&cellVoltageReg[i*(VOLTAGE_REG_SIZE + PEC_REG_SIZE)], 
                                      &ltc6802AllCellVoltages[i*NUM_CELLS]);
                
            }
            
            ltc6802State = LTC6802_WAIT;
            
            break;
            
        case LTC6802_WAIT:
        
            /* Empty 10 ms task manager cycles until 100 ms is reached again.
             * This is because AD conversions are variably dependent on number of
             * active boards and also takes longer than 10 ms */
            if(stateIterations > 0)
            {
                ltc6802State = LTC6802_WAIT;
            }
            else
            {
                /* Not 10 because it gets decremented at the end*/
                stateIterations = WAITING_TM_ITERATIONS + 1;
                ltc6802State = LTC6802_START_AD_CONVERSION;
            }
            
            break;
    }
    
    stateIterations--;
}


/* Store all voltage data from 18 8-bit voltage registers into 12 12-bit data types */
static void ltc6802_storeVoltages(uint8_t *cellVoltageReg, io_ltc6802_cellV *cellVoltages)
{
    uint8_t i, wordCount;
    wordCount = 0;

    /* Each voltage reading is 12 bits, and therefore two voltages
     * are stored evenly across three 8-bit registers.
     * PEC error reg is ignored here */
    for(i = 0; i < VOLTAGE_REG_SIZE -1; i+=3)
    {
        /* First cell reading in all of first reg and lower 4
        * bits of second reg */
        cellVoltages[wordCount].lowBits  = cellVoltageReg[i];
        cellVoltages[wordCount].highBits = cellVoltageReg[i+1];
        cellVoltages[wordCount].value    = cellVoltages[wordCount].value & LTC6802_VOLTAGE_BIT_MASK;
        wordCount++;
        
        /* Second cell reading in upper 4 bits of second reg
        * and all of third reg */
        cellVoltages[wordCount].lowBits  = cellVoltageReg[i+1];
        cellVoltages[wordCount].highBits = cellVoltageReg[i+2];
        cellVoltages[wordCount].value    = cellVoltages[wordCount].value >> LTC6802_VOLTAGE_BIT_SHIFT;
        wordCount++;
    }
    
    ltc6802_interpretVoltages(cellVoltages);
}

/* Convert raw cell data to voltage values */
static void ltc6802_interpretVoltages(io_ltc6802_cellV *cellVoltages)
{
    uint8_t i;
    for(i = 0; i < VOLTAGE_ARR_SIZE; i++)
    {
        /* LTC uses 1.5 mV per bit, accomplish this by multiplying by 3 and dividing by 2 
            (more efficient than floating point operations) */
        cellVoltages[i].value = (cellVoltages[i].value * 3);
        cellVoltages[i].value = (cellVoltages[i].value >> 1);
    }

}

/* Sends the command to start converting voltage readings to 
 * digital values in each register */
static void ltc6802_startVoltageADConvert()
{
    uint8_t command[1];
    command[0] = LTC6802_CMD_START_CELL_VOLTAGE_AD;
    
    io_spi_toggleCSLow(MC_BOARD_CS_P);
    io_spi_write(command, 1);
    io_spi_toggleCSHigh(MC_BOARD_CS_P);
}

/* For battery balancer to get voltage readings */
io_ltc6802_cellV* io_ltc6802_getCellVoltages()
{
    return &ltc6802AllCellVoltages;
}

/* Changes current configuration based which cells currently need to be discharged */
void io_ltc6802_updateDischargeStatus(io_ltc6802_discharge_cell_E *dcc)
{
    int8_t i = 0;
    int8_t j = 0;
    /* Set each discharge bit in the current config */
    for(i = 0; i < ltc6802TotalBoards; i++)
    {
        ltc6802Config[i].DCC1 = dcc[j];
        ltc6802Config[i].DCC2 = dcc[j+1];
        ltc6802Config[i].DCC3 = dcc[j+2];
        ltc6802Config[i].DCC4 = dcc[j+3];
        ltc6802Config[i].DCC5 = dcc[j+4];
        ltc6802Config[i].DCC6 = dcc[j+5];
        ltc6802Config[i].DCC7 = dcc[j+6];
        ltc6802Config[i].DCC8 = dcc[j+7];
        ltc6802Config[i].DCC9 = dcc[j+8];
        ltc6802Config[i].DCC10 = dcc[j+9];
        ltc6802Config[i].DCC11 = dcc[j+10];
        ltc6802Config[i].DCC12 = dcc[j+11];
        
        j += NUM_CELLS;
    }
    /* Write new discharge status to current config */
    ltc6802_writeConfig(ltc6802Config);
    
}


/* Tests if the configuration register can be written to and read from */
void io_ltc6802_testLTC(uint8_t numBoards)
{
    uint8_t i;
    /* Garbage values to make sure they properly get overwritten when grabbing
     data from the config register*/
    ltc6802_config_reg_group_S testReadConfig[IO_LTC6802_MAX_NUM_BOARDS];
    for(i=0; i < numBoards; i++)
    {
        testReadConfig[i].CFGR0 = 0xFF;
        testReadConfig[i].CFGR1 = 0xFF;
        testReadConfig[i].CFGR2 = 0xFF;
        testReadConfig[i].CFGR3 = 0xFF;
        testReadConfig[i].CFGR4 = 0xFF;
        testReadConfig[i].CFGR5 = 0xFF;
    }
    
    ltc6802_writeConfig(ltc6802Config);
    /* Read the configuration back */
    ltc6802_readConfig(testReadConfig);
    
    /* Check if the values match */
    for(i = 0; i < numBoards; i++)
    {
        DEBUG_ASSERT((testReadConfig[i].WDT), "WATCHDOG TRIPPED");
        DEBUG_ASSERT((!(testReadConfig[i].WDT)), "NO WATCHDOG ERRORS");

        DEBUG_ASSERT(((ltc6802Config[i].CFGR0 & 0x7F) == (testReadConfig[i].CFGR0 & 0x7F)), "CFGR0 ERROR");
        printf("Value written: %d\n\r", ltc6802Config[i].CFGR0);
        printf("Value read: %d\n\r", testReadConfig[i].CFGR0);
        DEBUG_ASSERT((ltc6802Config[i].CFGR1 == testReadConfig[i].CFGR1), "CFGR1 ERROR");
        printf("Value written: %d\n\r", ltc6802Config[i].CFGR1);
        printf("Value read: %d\n\r", testReadConfig[i].CFGR1);
        DEBUG_ASSERT((ltc6802Config[i].CFGR2 == testReadConfig[i].CFGR2), "CFGR2 ERROR");
        printf("Value written: %d\n\r", ltc6802Config[i].CFGR2);
        printf("Value read: %d\n\r", testReadConfig[i].CFGR2);
        DEBUG_ASSERT((ltc6802Config[i].CFGR3 == testReadConfig[i].CFGR3), "CFGR3 ERROR");
        printf("Value written: %d\n\r", ltc6802Config[i].CFGR3);
        printf("Value read: %d\n\r", testReadConfig[0].CFGR3);
        DEBUG_ASSERT((ltc6802Config[i].CFGR4 == testReadConfig[i].CFGR4), "CFGR4 ERROR");
        printf("Value written: %d\n\r", ltc6802Config[i].CFGR4);
        printf("Value read: %d\n\r", testReadConfig[i].CFGR4);
        DEBUG_ASSERT((ltc6802Config[i].CFGR5 == testReadConfig[i].CFGR5), "CFGR5 ERROR");
        printf("Value written: %d\n\r", ltc6802Config[i].CFGR5);
        printf("Value read: %d\n\r", testReadConfig[i].CFGR5);
    }

}
