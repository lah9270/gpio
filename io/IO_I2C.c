/*
 * File:    IO_I2C.c
 * Date:    12/30/16
 */

#include "IO_I2C.h"
#include "MC_I2C.h"
#include <string.h>

/* =============================================================================
 * Private Module constant definitions
 * =============================================================================
 */

#define PIC_SLAVE_ADDR      (0x01)  /* If we ever use slave mode in the future*/
#define I2C_MAX_QSIZE       (20)
#define I2C_ADDR_READ_BIT   (1)
#define I2C_ADDR_WRITE_BIT  (0)

/* =============================================================================
 * Private Enumerated type definitions
 * =============================================================================
 */

/* Possible states during an I2C transaction, as per SM diagrams */
typedef enum i2c_state_E 
{
    I2C_IDLE,
    I2C_DEQUEUE_TRB,
    I2C_STAGE_TRANSFER,
    I2C_TRANSFER_READ,
    I2C_TRANSFER_WRITE
	
} i2c_state_E;

/* Return status codes for FIFO queue operations */
typedef enum i2c_queue_status_E
{
    I2C_QUEUE_OP_SUCCESS = 0,
    I2C_QUEUE_FULL = 1,
    I2C_QUEUE_EMPTY = 2,

} i2c_queue_status_E;

/* Definitions of read/write transaction types within a TRB */
typedef enum i2c_request_type_E
{
    I2C_WRITE_REQ,
    I2C_READ_REQ

} i2c_request_type_E;

/* =============================================================================
 * Private structure definitions
 * =============================================================================
 */

/* Transaction Request Block: Packages the data for a single I2C transaction
 * We are assuming that by creating the TRB, the higher layer with which it
 * is associated already has a predefined memory space for its data buffer 
 */
typedef struct i2c_trb_S
{
    uint8_t             address;
    i2c_request_type_E  requestType;
    uint16_t            length;
    io_i2c_message_S    *pMessage;
	
} i2c_trb_S;

/* Queue structure for all I2C transactions */
typedef struct i2c_tx_queue_S
{
    i2c_trb_S       TRBList[I2C_MAX_QSIZE];
    int8_t			numInQueue;
    uint8_t			frontPos;
    uint8_t			backPos;

} i2c_tx_queue_S;

/* =============================================================================
 * Private module variables
 * =============================================================================
 */
bool i2cStateChanged;
int i2cNumRead;
int i2cNumWritten;
i2c_state_E i2cCurrentState;
i2c_state_E i2cNextState;
i2c_trb_S i2cCurrentTRB;
io_i2c_sm_status_E i2cCurrentStatus;
i2c_tx_queue_S i2cTXQueue;

/* =============================================================================
 * Private module function declarations
 * =============================================================================
 */
void i2c_processState();
void i2c_createTRB(uint8_t addr, uint16_t numBytes, io_i2c_message_S *newMessage, i2c_trb_S *newTRB);
i2c_queue_status_E i2c_enqueueTRB(i2c_trb_S* newTransaction);
i2c_queue_status_E i2c_dequeueTRB();

/* =============================================================================
 * Private module function definitions
 * =============================================================================
 */

/* Initialize IO layer of I2C protocol */
void io_i2c_init()
{
    /* Set baud rate */
    MC_I2C_SetBaud(MC_I2C_100KHZ);
    
    /* Initialize local variables */
    i2cStateChanged = false;
    i2cNumRead = 0;
    i2cNumWritten = 0;
    i2cCurrentState = I2C_IDLE;
    i2cNextState = I2C_IDLE;
    i2cCurrentStatus = IO_I2C_IDLE;
    
    /* Initialize I2C Queue */
    i2cTXQueue.numInQueue = 0;
    i2cTXQueue.frontPos = 0;
    i2cTXQueue.backPos = 0;
    
}

/* Function that is called during each iteration of Task Manager */
void io_i2c_stateMachine()
{
    /* Do one full run-through of the state machine to transmit one
     * full set of data from the queue */
    do
    {
        /* Tasks related to current state */
        i2c_processState();
        if (i2cStateChanged) 
        {
            /* Prep the next state */
            i2cCurrentState = i2cNextState;
            i2cStateChanged = false;
        }
        
        /* If error state, abort transfer */
        if(i2cCurrentStatus == IO_I2C_NO_ACK)
        {
            i2cCurrentState = I2C_IDLE;
            i2cNextState = I2C_IDLE;
        }
    }while(i2cCurrentState != I2C_IDLE);

}

/* Perform tasks necessary for completion of each state */
void i2c_processState() 
{ 
    switch (i2cCurrentState) 
    {       
        case I2C_IDLE:
            i2cCurrentStatus = IO_I2C_IDLE;
            /* If a TRB has not been created then remain at idle state */
            if (i2cTXQueue.numInQueue != 0) 
            {
                i2cNextState = I2C_DEQUEUE_TRB;
                i2cStateChanged = true;
            }
            break;

        case I2C_DEQUEUE_TRB:
            i2cCurrentStatus = IO_I2C_RUNNING;
            /* Dequeue the TRB */
            i2c_dequeueTRB();
            i2cCurrentTRB.pMessage->messageStatus = IO_I2C_IN_PROGRESS;
            /* Check if lines are active */
            while(!MC_I2C_IsIdle())
            {
                /* Intentional null statement */
            }
            
            /* Generate the START condition */
            MC_I2C_START();

            /* Wait until ready */
            while(!MC_I2C_TxRdy())
            {
                /* Intentional NULL statement */
            }
            
            /* Transmit the address and stage the next state change */
            MC_I2C_Tx(i2cCurrentTRB.address);
            i2cNextState = I2C_STAGE_TRANSFER;
            i2cStateChanged = true;
            
            break;
            
        case I2C_STAGE_TRANSFER:
            /* Wait for the line to become open */
            while(!MC_I2C_IsIdle())
            {
                /* Intentional NULL statement */
            }
            /* Check if Tx is complete */
            while (!MC_I2C_TxComplete())
            {
                /* Intentional NULL statement */
            }
            /* Check if ACK was received */
            if (MC_I2C_ACKReceived()) 
            {
                /* Check if TRB is a read (1 = Read, 0 = Write) */
                if (i2cCurrentTRB.requestType == I2C_READ_REQ) 
                {
                    i2cNextState = I2C_TRANSFER_READ;
                    i2cStateChanged = true;
                } 
                else
                {
                    i2cNextState = I2C_TRANSFER_WRITE;
                    i2cStateChanged = true;
                }
            } 
            else 
            {
                /* No ACK received */
                i2cCurrentTRB.pMessage->messageStatus = IO_I2C_MISSING_ACK;
                i2cCurrentStatus = IO_I2C_NO_ACK;
            }

            break;

        case I2C_TRANSFER_READ:
            /* Read and store a new byte of data */
            MC_I2C_BeginRx();
            /* Wait until ready */
            while (!MC_I2C_RxRdy())
            {
                /* Intentional NULL statement */
            }
            
            /* Read 1 byte of data */
            i2cCurrentTRB.pMessage->pBuffer[i2cNumRead] = MC_I2C_Rx();

            /* Increment number of bytes read */
            i2cNumRead++;

            /* At the end of transaction? */
            if (i2cNumRead == i2cCurrentTRB.length || 
                i2cCurrentTRB.pMessage->messageComplete(i2cCurrentTRB.pMessage->pBuffer)) 
            {
                /* Yes - Finish transfer and reset byte counter */
                MC_I2C_STOP();
                i2cNumRead = 0;
                i2cNextState = I2C_IDLE;
                i2cStateChanged = true;
            } 
            else 
            {
                /* No - Generate ACK */
                MC_I2C_ACK();
            }

            break;

        case I2C_TRANSFER_WRITE:
            while(!MC_I2C_TxRdy())
            {
                /* Intentional NULL statement */
            }
            /* Send next byte of data */
            MC_I2C_Tx(i2cCurrentTRB.pMessage->pBuffer[i2cNumWritten]);
            /* Wait for the line to become idle */
            while(!MC_I2C_IsIdle())
            {
                /* Intentional NULL statement */
            }
            /* Check if Tx is complete */
            while (!MC_I2C_TxComplete())
            {
                /* Intentional NULL statement */
            }
            /* Increment number of bytes written */
            i2cNumWritten++;

            /* At the end? */
            if (i2cNumWritten == i2cCurrentTRB.length ||
                i2cCurrentTRB.pMessage->messageComplete(i2cCurrentTRB.pMessage->pBuffer)) 
            {
                /* Yes - finish transfer and reset byte counter */
                MC_I2C_STOP();
                i2cNumWritten = 0;
                i2cNextState = I2C_IDLE;
                i2cStateChanged = true;
            } 
            else 
            {
                /* No - Check ACK received */
                if (!MC_I2C_ACKReceived()) 
                {
                    /* Transfer failed */
                    i2cCurrentTRB.pMessage->messageStatus = IO_I2C_MISSING_ACK;
                    i2cCurrentStatus = IO_I2C_NO_ACK;
                }
            }

            break;
    }
}

/* Function to be called from higher layers to perform a read operation */
void io_i2c_read(uint8_t addr, uint8_t numBytes, io_i2c_message_S *newMessage) 
{
    uint8_t readAddr;
    readAddr = ((addr << 1) + I2C_ADDR_READ_BIT);
    i2c_trb_S newTRB;
	i2c_createTRB(readAddr, numBytes, newMessage, &newTRB);
    i2c_enqueueTRB(&newTRB);

}

/* Function to be called from higher layers to perform a write operation */
void io_i2c_write(uint8_t addr, uint8_t numBytes, io_i2c_message_S *newMessage) 
{
    uint8_t writeAddr = ((addr << 1) + I2C_ADDR_WRITE_BIT);
    i2c_trb_S newTRB;
	i2c_createTRB(writeAddr, numBytes, newMessage, &newTRB);
    i2c_enqueueTRB(&newTRB);
}

/* Creates a transaction request block to encapsulate one I2C transaction */

/* Assumes the address is pre-adjusted for read and write operations*/
void i2c_createTRB(uint8_t addr, uint16_t numBytes, io_i2c_message_S *newMessage, i2c_trb_S *newTRB)
{
    newTRB->address = addr;
    if (addr & 1) 
    {
        newTRB->requestType = I2C_READ_REQ;
    } else 
    {
        newTRB->requestType = I2C_WRITE_REQ;
    }
    newTRB->length = numBytes;
    newTRB->pMessage = newMessage;
    newTRB->pMessage->messageStatus = IO_I2C_PENDING;

}

/* Inserts 1 new Transaction Request Block to the end of the queue */
i2c_queue_status_E i2c_enqueueTRB(i2c_trb_S *newTransaction)
{
    i2c_queue_status_E returnStatus = I2C_QUEUE_OP_SUCCESS;
    
    /* Can't enqueue into a full queue */
    if (i2cTXQueue.numInQueue >= I2C_MAX_QSIZE)
    {
        returnStatus = I2C_QUEUE_FULL;
    }
    else
    {
        /* Enqueue the transaction block at the next available position */
        /*memcpy(DEST, SRC, SIZE);*/
        memcpy(&i2cTXQueue.TRBList[i2cTXQueue.frontPos], newTransaction, sizeof(i2c_trb_S));
        newTransaction->pMessage->messageStatus = IO_I2C_IN_QUEUE;
		
		/* Incremet the entry point and the number in the queue*/
        i2cTXQueue.frontPos++;
        i2cTXQueue.numInQueue++;

        if (i2cTXQueue.frontPos == I2C_MAX_QSIZE)
        {
            i2cTXQueue.frontPos = 0;
        }
    }
    return returnStatus;
}

/* Dequeue 1 Transaction Request Block from the front of the queue */
i2c_queue_status_E i2c_dequeueTRB()
{
    i2c_queue_status_E returnStatus = I2C_QUEUE_OP_SUCCESS;
    
    /* Can't dequeue from an empty queue */
    if (i2cTXQueue.numInQueue <= 0) 
    {
        returnStatus = I2C_QUEUE_EMPTY;
    }
    else
    {
        /*Dequeue the next transaction block*/
        /*memcpy(DEST, SRC, SIZE);*/
		memcpy(&i2cCurrentTRB, &i2cTXQueue.TRBList[i2cTXQueue.backPos], sizeof(i2c_trb_S));
        
        /* Increment the exit position and decrement the amount in the queue*/
        i2cTXQueue.backPos++;
        i2cTXQueue.numInQueue--;

        if (i2cTXQueue.backPos == I2C_MAX_QSIZE)
        {
            i2cTXQueue.backPos = 0;
        }
    }
    return returnStatus;
}

/* Returns current status of I2C state machine */
io_i2c_sm_status_E io_i2c_getStatus()
{
    return i2cCurrentStatus;
}

/* TEMPORARY FUNCTION FOR THE SAKE OF DEBUGGING*/
/* RETURNS THE CURRENT STATE OF THE STATE MACHINE*/
i2c_state_E io_i2c_getState() 
{
    return i2cCurrentState;
}

