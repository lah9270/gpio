/*
 * File:    IO_SPI.c
 * Created: 3/24/2017
*/

#include "IO_SPI.h"
#include "MC_SPI.h"
#include "MC_GPIO.h"

/* ========================================================================
 * Private Macro Definitions
 * ========================================================================
*/

/* ===========================================================================================================================
 * Private module variables
 * ===========================================================================================================================
*/
io_spi_sync_modes_E spiRequestedSyncMode;
io_spi_bus_modes_E spiRequestedBusMode;
io_spi_sample_modes_E spiRequestedSampleMode;

io_spi_sync_modes_E spiSyncMode;
io_spi_bus_modes_E spiBusMode;
io_spi_sample_modes_E spiSampleMode;

/* ===========================================================================================================================
 * Private module function declarations
 * ===========================================================================================================================
*/

/* ===========================================================================================================================
 *  Module function definitions
 * ===========================================================================================================================
*/

void io_spi_setRequestedSyncMode(io_spi_sync_modes_E syncMode)
{
    spiRequestedSyncMode = syncMode;
}

void io_spi_setRequestedBusMode(io_spi_bus_modes_E busMode)
{
    spiRequestedBusMode = busMode;
}

void io_spi_setRequestedSampleMode(io_spi_sample_modes_E sampleMode)
{
    spiRequestedSampleMode = sampleMode;
}

void io_spi_init()
{   
    /* Set new initialization variables */
    mc_spi_setRequestedSyncMode((mc_spi_sync_modes_E)spiRequestedSyncMode);
    mc_spi_setRequestedBusMode((mc_spi_bus_modes_E)spiRequestedBusMode);
    mc_spi_setRequestedSampleMode((mc_spi_sample_modes_E)spiRequestedSampleMode);
    
    /* Don't open the mc spi port, the task manager will take care of that */
    
    /* Set the current states to all default as we can't be sure that they are set */
    spiSyncMode = IO_SPI_SYNC_DEFAULT;
    spiBusMode = IO_SPI_MODE_DEFAULT;
    spiSampleMode = IO_SPI_SAMPLE_DEFAULT;
}

void io_spi_close()
{
    spiRequestedSyncMode = IO_SPI_SYNC_DEFAULT;
    spiRequestedBusMode = IO_SPI_MODE_DEFAULT;
    spiRequestedSampleMode = IO_SPI_SAMPLE_DEFAULT; 
    
    mc_spi_close();
}

void io_spi_reopen(io_spi_sync_modes_E syncMode, io_spi_bus_modes_E busMode, io_spi_sample_modes_E sampleMode)
{
    /* Set new initialization variables */
    io_spi_setRequestedSyncMode(syncMode);
    io_spi_setRequestedBusMode(busMode);
    io_spi_setRequestedSampleMode(sampleMode);
    
    /* Reopen the MC layer SPI channel */
    mc_spi_reopen( (mc_spi_sync_modes_E)spiRequestedSyncMode, (mc_spi_bus_modes_E)spiRequestedBusMode, (mc_spi_sample_modes_E)spiRequestedSampleMode );
    
    /* Set the current states to the new values */
    spiSyncMode = syncMode;
    spiBusMode = busMode;
    spiSampleMode = sampleMode;
}

void io_spi_openCS(MC_PIN_E csPin)
{
    MC_pin_setDirection(csPin, MC_PIN_OUTPUT);
    //make sure CS starts high
    MC_GPIO_write(csPin, MC_PIN_HIGH);
}

void io_spi_read(unsigned char * data_in, unsigned int data_len)
{
    int i;
    for(i = 0; i < data_len; i++)
    {
        data_in[i] = mc_spi_readChar();
    }
}

void io_spi_write(unsigned char * data_out, unsigned int data_len)
{
    int i;
    for(i = 0; i < data_len; i++)
    {
        mc_spi_writeChar(data_out[i]);
    }
}

void io_spi_toggleCSLow(MC_PIN_E csPin)
{
    MC_GPIO_write(csPin, MC_PIN_LOW);
}

void io_spi_toggleCSHigh(MC_PIN_E csPin)
{
    MC_GPIO_write(csPin, MC_PIN_HIGH);
}


io_spi_sync_modes_E io_spi_getSyncMode()
{
    return spiSyncMode;
}

io_spi_bus_modes_E io_spi_getBusMode()
{
    return spiBusMode;
}

io_spi_sample_modes_E io_spi_getSampleMode()
{
    return spiSampleMode;
}

io_spi_sync_modes_E io_spi_getRequestedSyncMode()
{
    return spiRequestedSyncMode;
}

io_spi_bus_modes_E io_spi_getRequestedBusMode()
{
    return spiRequestedBusMode;
}

io_spi_sample_modes_E io_spi_getRequestedSampleMode()
{
    return spiRequestedSampleMode;
}