/*
 * File: IO_networkManager.h
 * Created: 4/1/2017
 *
 * Responsible for collecting data from other applications and packaging the
 * data into formatted frames for transmission on the CAN bus.
*/

#ifndef IO_NETWORKMANAGER_H
#define IO_NETWORKMANAGER_H

#include <EVT_std_includes.h>

/* ============================================================================
 * Public Enumerated Type Definitions
 * ============================================================================
*/
typedef enum io_network_message_E
{
    IO_NETWORK_GTW_REQUESTS = 16,
    IO_NETWORK_IMU_ACCELERATIONS = 1280,
    IO_NETWORK_IMU_ANGULAR_POSITIONS = 1282,
    IO_NETWORK_IMU_GYRO = 1283,
    IO_NETWORK_IMU_MAGNETOMETER = 1284,
    IO_NETWORK_IMU_ORIENTATION = 1285,
    IO_NETWORK_IMU_ALTIMETER = 1281,
    IO_NETWORK_BMS0_MODULE_VOLTAGES1 = 132,
    IO_NETWORK_BMS0_MODULE_VOLTAGES2 = 133,
    IO_NETWORK_BMS0_MODULE_VOLTAGES3 = 134,
    IO_NETWORK_BMS0_MODULE_VOLTAGES4 = 135,
    IO_NETWORK_BMS0_MODULE_VOLTAGES5 = 136,
    IO_NETWORK_BMS0_MODULE_VOLTAGES6 = 137,
    IO_NETWORK_BMS0_MODULE_VOLTAGES7 = 138,
    IO_NETWORK_BMS0_MODULE_VOLTAGES8 = 139,
    IO_NETWORK_BMS0_PACK_INFO = 130,
    IO_NETWORK_BMS0_TEMP1 = 128,
    IO_NETWORK_BMS0_TEMP2 = 129,
    IO_NETWORK_BMS1_TEMP1 = 256,
    IO_NETWORK_BMS1_TEMP2 = 257,
    IO_NETWORK_BMS2_TEMP1 = 384,
    IO_NETWORK_BMS2_TEMP2 = 385,
    IO_NETWORK_BMS3_TEMP1 = 512,
    IO_NETWORK_BMS3_TEMP2 = 513,
    IO_NETWORK_BMS0_MODULE_DISCHARGE = 131,
} io_network_message_E;


/* ============================================================================
 * Public Structure Definitions
 * ============================================================================
*/

#ifdef __DEVICE_GTW__
typedef struct io_network_GTW_requests_S
{
    int8_t transmit_request;
    int8_t bms_state_request;
} io_network_GTW_requests_S;
#endif /* __DEVICE_GTW__ */

#ifdef __DEVICE_IMU__
typedef struct io_network_IMU_accelerations_S
{
    uint16_t accel_x;
    uint16_t accel_y;
    uint16_t accel_z;
    int8_t shake_accel;
} io_network_IMU_accelerations_S;
#endif /* __DEVICE_IMU__ */

#ifdef __DEVICE_IMU__
typedef struct io_network_IMU_angular_positions_S
{
    uint16_t pitch_y;
    uint16_t roll_x;
    int16_t yaw_z;
} io_network_IMU_angular_positions_S;
#endif /* __DEVICE_IMU__ */

#ifdef __DEVICE_IMU__
typedef struct io_network_IMU_gyro_S
{
    uint16_t gyro_x;
    uint16_t gyro_y;
    uint16_t gyro_z;
    int8_t shake_gyro;
} io_network_IMU_gyro_S;
#endif /* __DEVICE_IMU__ */

#ifdef __DEVICE_IMU__
typedef struct io_network_IMU_magnetometer_S
{
    uint16_t flux_x;
    uint16_t flux_y;
    uint16_t flux_z;
    int16_t north;
} io_network_IMU_magnetometer_S;
#endif /* __DEVICE_IMU__ */

#ifdef __DEVICE_IMU__
typedef struct io_network_IMU_orientation_S
{
    uint16_t orientation_x;
    uint16_t orientation_y;
    uint16_t orientation_z;
    uint16_t orientation_w;
} io_network_IMU_orientation_S;
#endif /* __DEVICE_IMU__ */

#ifdef __DEVICE_IMU__
typedef struct io_network_IMU_altimeter_S
{
    uint32_t altitude;
    uint16_t imu_ambient_temp;
} io_network_IMU_altimeter_S;
#endif /* __DEVICE_IMU__ */

#ifdef __DEVICE_BMS0__
typedef struct io_network_BMS0_module_voltages1_S
{
    int16_t module_v1;
    int16_t module_v2;
    int16_t module_v3;
    int16_t module_v4;
} io_network_BMS0_module_voltages1_S;
#endif /* __DEVICE_BMS0__ */

#ifdef __DEVICE_BMS0__
typedef struct io_network_BMS0_module_voltages2_S
{
    int16_t module_v5;
    int16_t module_v6;
    int16_t module_v7;
    int16_t module_v8;
} io_network_BMS0_module_voltages2_S;
#endif /* __DEVICE_BMS0__ */

#ifdef __DEVICE_BMS0__
typedef struct io_network_BMS0_module_voltages3_S
{
    int16_t module_v9;
    int16_t module_v10;
    int16_t module_v11;
    int16_t module_v12;
} io_network_BMS0_module_voltages3_S;
#endif /* __DEVICE_BMS0__ */

#ifdef __DEVICE_BMS0__
typedef struct io_network_BMS0_module_voltages4_S
{
    int16_t module_v13;
    int16_t module_v14;
    int16_t module_v15;
    int16_t module_v16;
} io_network_BMS0_module_voltages4_S;
#endif /* __DEVICE_BMS0__ */

#ifdef __DEVICE_BMS0__
typedef struct io_network_BMS0_module_voltages5_S
{
    int16_t module_v17;
    int16_t module_v18;
    int16_t module_v19;
    int16_t module_v20;
} io_network_BMS0_module_voltages5_S;
#endif /* __DEVICE_BMS0__ */

#ifdef __DEVICE_BMS0__
typedef struct io_network_BMS0_module_voltages6_S
{
    int16_t module_v21;
    int16_t module_v22;
    int16_t module_v23;
    int16_t module_v24;
} io_network_BMS0_module_voltages6_S;
#endif /* __DEVICE_BMS0__ */

#ifdef __DEVICE_BMS0__
typedef struct io_network_BMS0_module_voltages7_S
{
    int16_t module_v25;
    int16_t module_v26;
    int16_t module_v27;
    int16_t module_v28;
} io_network_BMS0_module_voltages7_S;
#endif /* __DEVICE_BMS0__ */

#ifdef __DEVICE_BMS0__
typedef struct io_network_BMS0_module_voltages8_S
{
    int16_t module_v29;
    int16_t module_v30;
} io_network_BMS0_module_voltages8_S;
#endif /* __DEVICE_BMS0__ */

#ifdef __DEVICE_BMS0__
typedef struct io_network_BMS0_pack_info_S
{
    int16_t pack_voltage;
    uint16_t pack_current;
    uint32_t pack_power;
    int8_t pack_soc;
} io_network_BMS0_pack_info_S;
#endif /* __DEVICE_BMS0__ */

#ifdef __DEVICE_BMS0__
typedef struct io_network_BMS0_temp1_S
{
    int8_t cell_temp1;
    int8_t cell_temp2;
    int8_t cell_temp3;
    int8_t cell_temp4;
    int8_t cell_temp5;
    int8_t cell_temp6;
    int8_t cell_temp7;
    int8_t cell_temp8;
} io_network_BMS0_temp1_S;
#endif /* __DEVICE_BMS0__ */

#ifdef __DEVICE_BMS0__
typedef struct io_network_BMS0_temp2_S
{
    int8_t cell_temp9;
    int8_t cell_temp10;
    int8_t cell_temp11;
    int8_t cell_temp12;
    int8_t cell_temp13;
    int8_t cell_temp14;
    int8_t cell_temp15;
    uint8_t bms0_ambient_temp;
} io_network_BMS0_temp2_S;
#endif /* __DEVICE_BMS0__ */

#ifdef __DEVICE_BMS1__
typedef struct io_network_BMS1_temp1_S
{
    int8_t cell_temp16;
    int8_t cell_temp17;
    int8_t cell_temp18;
    int8_t cell_temp19;
    int8_t cell_temp20;
    int8_t cell_temp21;
    int8_t cell_temp22;
    int8_t cell_temp23;
} io_network_BMS1_temp1_S;
#endif /* __DEVICE_BMS1__ */

#ifdef __DEVICE_BMS1__
typedef struct io_network_BMS1_temp2_S
{
    int8_t cell_temp24;
    int8_t cell_temp25;
    int8_t cell_temp26;
    int8_t cell_temp27;
    int8_t cell_temp28;
    int8_t cell_temp29;
    int8_t cell_temp30;
    uint8_t bms1_ambient_temp;
} io_network_BMS1_temp2_S;
#endif /* __DEVICE_BMS1__ */

#ifdef __DEVICE_BMS2__
typedef struct io_network_BMS2_temp1_S
{
    int8_t cell_temp31;
    int8_t cell_temp32;
    int8_t cell_temp33;
    int8_t cell_temp34;
    int8_t cell_temp35;
    int8_t cell_temp36;
    int8_t cell_temp37;
    int8_t cell_temp38;
} io_network_BMS2_temp1_S;
#endif /* __DEVICE_BMS2__ */

#ifdef __DEVICE_BMS2__
typedef struct io_network_BMS2_temp2_S
{
    int8_t cell_temp39;
    int8_t cell_temp40;
    int8_t cell_temp41;
    int8_t cell_temp42;
    int8_t cell_temp43;
    int8_t cell_temp44;
    int8_t cell_temp45;
    uint8_t bms2_ambient_temp;
} io_network_BMS2_temp2_S;
#endif /* __DEVICE_BMS2__ */

#ifdef __DEVICE_BMS3__
typedef struct io_network_BMS3_temp1_S
{
    int8_t cell_temp46;
    int8_t cell_temp47;
    int8_t cell_temp48;
    int8_t cell_temp49;
    int8_t cell_temp50;
    int8_t cell_temp51;
    int8_t cell_temp52;
    int8_t cell_temp53;
} io_network_BMS3_temp1_S;
#endif /* __DEVICE_BMS3__ */

#ifdef __DEVICE_BMS3__
typedef struct io_network_BMS3_temp2_S
{
    int8_t cell_temp54;
    int8_t cell_temp55;
    int8_t cell_temp56;
    int8_t cell_temp57;
    int8_t cell_temp58;
    int8_t cell_temp59;
    int8_t cell_temp60;
    uint8_t bms3_ambient_temp;
} io_network_BMS3_temp2_S;
#endif /* __DEVICE_BMS3__ */

#ifdef __DEVICE_BMS0__
typedef struct io_network_BMS0_module_discharge_S
{
    int8_t module_discharge_1;
    int8_t module_discharge_2;
    int8_t module_discharge_3;
    int8_t module_discharge_4;
    int8_t module_discharge_5;
    int8_t module_discharge_6;
    int8_t module_discharge_7;
    int8_t module_discharge_8;
    int8_t module_discharge_9;
    int8_t module_discharge_10;
    int8_t module_discharge_11;
    int8_t module_discharge_12;
    int8_t module_discharge_13;
    int8_t module_discharge_14;
    int8_t module_discharge_15;
    int8_t module_discharge_16;
    int8_t module_discharge_17;
    int8_t module_discharge_18;
    int8_t module_discharge_19;
    int8_t module_discharge_20;
    int8_t module_discharge_21;
    int8_t module_discharge_22;
    int8_t module_discharge_23;
    int8_t module_discharge_24;
    int8_t module_discharge_25;
    int8_t module_discharge_26;
    int8_t module_discharge_27;
    int8_t module_discharge_28;
    int8_t module_discharge_29;
    int8_t module_discharge_30;
} io_network_BMS0_module_discharge_S;
#endif /* __DEVICE_BMS0__ */


/* ============================================================================
 * Public Function Declarations
 * ============================================================================
*/

/* Initializes the network manager IO layer. */
void io_networkManager_init();


/* Instructs the network manager to package a CAN data frame formatted based on
 * a CAN ID with data collected from an application and send it.
 *
 * Arguments:
 *  - id: The CAN message's bus ID.
 *  - data: A pointer to the collected data to package.
 */
void io_networkManager_processFrame(uint16_t id, void * data);

#endif /* IO_NETWORKMANAGER_H */
