/*
 * File:    IO_I2C.h
 * Date:    12/30/16
 */

#ifndef IO_I2C_H
#define IO_I2C_H

#include "EVT_std_includes.h"

/* =============================================================================
 * Public Enumerated type definitions
 * =============================================================================
 */

/* Message status for higher layers using the I2C protocol */
typedef enum io_i2c_status_E
{
    IO_I2C_MISSING_ACK,
    IO_I2C_PENDING,
    IO_I2C_IN_QUEUE,
    IO_I2C_IN_PROGRESS,
    IO_I2C_SUCCESS

} io_i2c_status_E;

typedef enum io_i2c_sm_status_E
{
    IO_I2C_NO_ACK,
    IO_I2C_IDLE,
    IO_I2C_RUNNING

} io_i2c_sm_status_E;

/* =============================================================================
 * Public Structure definitions
 * =============================================================================
 */

/* Structure that is created by upper layers to pass into the IO layer. Becomes
 * encapsulated in a TRB to be placed on the transmission queue. Contains a
 * pointer to a data buffer, an error status code, and a function pointer that 
 * determines if the transmission is complete (used for reads of indeterminate 
 * lengths.
 */
typedef struct io_i2c_message_S
{
    uint8_t             *pBuffer;
    io_i2c_status_E     messageStatus;
    bool                (*messageComplete)(uint8_t *);
    
} io_i2c_message_S;
/* =============================================================================
 * Public function declarations
 * =============================================================================
 */

/* Name:
 *      io_i2c_init
 * Parameters:
 *      NONE
 * Returns:
 *      VOID
 * Description
 *      Initialize the IO layer of the I2C protocol. Sets baud rate
 *      and initializes local variables and the FIFO queue.
 */
void io_i2c_init();

/* Name:
 *      io_i2c_stateMachine
 * Parameters:
 *      NONE
 * Returns:
 *      VOID
 * Description
 *      Function to be called by higher layers to complete
 *      one full iteration of the I2C state machine. If there are
 *      pending data transfers, one transfer will take place per
 *      iteration of the state machine
 */
void io_i2c_stateMachine();

/* Name:
 *      io_i2c_read
 * Parameters:
 *      addr: UNSHIFTED I2C address of slave device to be read from
 *      numBytes: Desired number of bytes to be read from slave
 *      *pbuffer: Pointer to a byte array that stores the read data
 * Returns:
 *      VOID
 * Description:
 *      Shifts the given address and sets the first bit to a 1, indicating
 *      a read as per the I2C protocol. Successively calls io_i2c_createTRB()
 *      and io_i2c_enqueueTRB() to create a new transaction block and place it
 *      on the active queue of pending data transmissions.
 */
void io_i2c_read(uint8_t addr, uint8_t numBytes, io_i2c_message_S *newMessage);

/* Name:
 *      io_i2c_write
 * Parameters:
 *      addr: UNSHIFTED I2C address of slave device to be written to
 *      numBytes: Desired number of bytes to be written to slave
 *      *pbuffer: Pointer to a byte array that stores the write data
 * Returns:
 *      VOID
 * Description:
 *      Shifts the given address and sets the first bit to a 0, indicating
 *      a write as per the I2C protocol. Successively calls io_i2c_createTRB()
 *      and io_i2c_enqueueTRB() to create a new transaction block and place it
 *      on the active queue of pending data transmissions.
 */
void io_i2c_write(uint8_t addr, uint8_t numBytes, io_i2c_message_S *newMessage);

/* Name:
 *      io_i2c_getStatus
 * Parameters:
 *      NONE
 * Returns:
 *      io_i2c_sm_status_E
 * Description:
 *      Returns current status of state machine for error checking
 *      purposes
 */
io_i2c_sm_status_E io_i2c_getStatus();
#endif
