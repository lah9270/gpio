/*
 * File:    IO_SPI.h
 * Created: 3/24/2017
*/

#ifndef IO_SPI_H
#define IO_SPI_H

#include "EVT_std_includes.h"
#include "MC_pin.h"

/* ===========================================================================================================================
 * Public precompiler definitions
 * ===========================================================================================================================
*/


/* ===========================================================================================================================
 * Public Enumerated type definitions
 * ===========================================================================================================================
*/

typedef enum io_spi_sync_modes_E
/* This enum is intentionally matched with mc_spi_sync_modes_E in MC_SPI.h
 * Do not modify without modifying both
 */
{
    IO_SPI_SYNC_DEFAULT = 0u,
    IO_SPI_FOSC_4    = 0u,      // SPI Master mode, clock = Fosc/4
    IO_SPI_FOSC_16   = 1u,      // SPI Master mode, clock = Fosc/16
    IO_SPI_FOSC_64   = 2u,      // SPI Master mode, clock = Fosc/64
    IO_SPI_FOSC_TMR2 = 3u,      // SPI Master mode, clock = TMR2 output/2
    IO_SLV_SSON      = 4u,      // SPI Slave mode, /SS pin control enabled
    IO_SLV_SSOFF     = 5u,      // SPI Slave mode, /SS pin control disabled
    IO_SPI_FOSC_8    = 10u,     // SPI Master mode, clock = Fosc/8
} io_spi_sync_modes_E;

typedef enum io_spi_bus_modes_E
/* This enum is intentionally matched with mc_spi_bus_modes_E in MC_SPI.h
 * Do not modify without modifying both
 */
{
    IO_SPI_MODE_DEFAULT = 0u,
    
    /* Setting for SPI bus Mode 0,0
     *     CKE = 1 Transmit occurs on transition from active to Idle clock state 
     *     CKP = 0 Idle state for clock is a low level
     */
    IO_SPI_MODE_00      = 0u,
    
    /* Setting for SPI bus Mode 0,1
     *     CKE = 0 Transmit occurs on transition from Idle to active clock state 
     *     CKP = 0 Idle state for clock is a low level
     */
    IO_SPI_MODE_01      = 1u,
    
    /* Setting for SPI bus Mode 1,0
     *     CKE = 1 Transmit occurs on transition from active to Idle clock state
     *     CKP = 1 Idle state for clock is a high level
     */
    IO_SPI_MODE_10      = 2u,
    
    /* Setting for SPI bus Mode 1,1
     *     CKE = 0 Transmit occurs on transition from Idle to active clock state
     *     CKP = 1 Idle state for clock is a high level
     */
    IO_SPI_MODE_11      = 3u,
} io_spi_bus_modes_E;

// Master SPI mode only
typedef enum io_spi_sample_modes_E
/* This enum is intentionally matched with mc_spi_sample_modes_E in MC_SPI.h
 * Do not modify without modifying both
 */
{
    IO_SPI_SAMPLE_DEFAULT = 0u,
    IO_SPI_SAMPLE_MID     = 0u,   // Input data sample at middle of data out
    IO_SPI_SAMPLE_END     = 128u, // Input data sample at end of data out
} io_spi_sample_modes_E;

/* ===========================================================================================================================
 * Public Structure definitions
 * ===========================================================================================================================
*/


/* ===========================================================================================================================
 * Public function declarations
 * ===========================================================================================================================
*/

/* These 3 functions set the configuration parameters that are needed to open the SPI port.
 * These MUST be set before calling io_spi_init() or it will open a port with undefined properties
 * 
 * syncMode   : The synchronization mode to use. Also sets whether Master or Slave 
*  busMode    : The SPI bus mode to use. Sets clock and rising/falling edge mode
*  sampleMode : Sample input in the middle of the data out or the end
 */
void io_spi_setRequestedSyncMode(io_spi_sync_modes_E syncMode);
void io_spi_setRequestedBusMode(io_spi_bus_modes_E busMode);
void io_spi_setRequestedSampleMode(io_spi_sample_modes_E sampleMode);

/* These 3 functions are called to retrieve the current configuration of the SPI port */
io_spi_sync_modes_E io_spi_getSyncMode();
io_spi_bus_modes_E io_spi_getBusMode();
io_spi_sample_modes_E io_spi_getSampleMode();

/* For modules that need SPI frequently re-opened (the LTC), get the values passed in during initialization in IO.c*/
io_spi_sync_modes_E io_spi_getRequestedSyncMode();
io_spi_bus_modes_E io_spi_getRequestedBusMode();
io_spi_sample_modes_E io_spi_getRequestedSampleMode();

/***********************************************************************************
* Name        : io_spi_init
* Description : Initializes the SPI port using the values set by the setter functions
* Pre         : Setters are all called and set
* @param      : None
* @return     : None
***********************************************************************************/
void io_spi_init();

/***********************************************************************************
* Name        : io_spi_reopen
* Description : Initializes the SPI port using the parameter values
* Pre         : None
* @param      : syncMode   : The synchronization mode to use. Also sets whether Master or Slave 
*               busMode    : The SPI bus mode to use. Sets clock and rising/falling edge mode
*               sampleMode : Sample input in the middle of the data out or the end
* @return     : None
***********************************************************************************/
void io_spi_reopen(io_spi_sync_modes_E syncMode, io_spi_bus_modes_E busMode, io_spi_sample_modes_E sampleMode);

/***********************************************************************************
* Name        : io_spi_openCS
* Description : Initializes the Chip Select Pin as an output and sets it high
* Pre         : None
* @param      : csPin : The Chip Select pin to be initialized
* @return     : None
***********************************************************************************/
void io_spi_openCS(MC_PIN_E csPin);

/***********************************************************************************
* Name        : io_spi_read
* Description : Reads a char array from the SPI bus. The caller needs to handle CS themselves
* Pre         : SPI is initialized
* @param      : data_in: init'ed array to be populated with SPI data
*				data_len: Length of data array to be populated
* @return     : None
***********************************************************************************/
void io_spi_read(unsigned char * data_in, unsigned int data_len);

/***********************************************************************************
* Name        : io_spi_write
* Description : Writes a char array onto the SPI bus. The caller needs to handle CS themselves
* Pre         : SPI is initialized
* @param      : data_out: Data to be sent over SPI
*				data_len: Length of data array
* @return     : None
***********************************************************************************/
void io_spi_write(unsigned char * data_out, unsigned int data_len);

/***********************************************************************************
* Name        : io_spi_toggleCSLow
* Description : Writes the Chip Select Pin low
* Pre         : None
* @param      : csPin : The Chip Select pin to be written to
* @return     : None
***********************************************************************************/
void io_spi_toggleCSLow(MC_PIN_E csPin);

/***********************************************************************************
* Name        : io_spi_toggleCSHigh
* Description : Writes the Chip Select Pin high
* Pre         : None
* @param      : csPin : The Chip Select pin to be written to
* @return     : None
***********************************************************************************/
void io_spi_toggleCSHigh(MC_PIN_E csPin);

/***********************************************************************************
* Name        : io_spi_close
* Description : closes SPI. Sets SPI pins as GPIO. Clears the values set by io_spi_open()
* @param      : None
* @return     : None
***********************************************************************************/
void io_spi_close();

#endif 	/* IO_SPI_H */