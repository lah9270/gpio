/* 
 * File:   MC.c
 *
 * Created on September 8, 2016, 8:07 PM
 */

#include "MC.h"
#include "MC_CAN.h"
#include "MC_UART.h"

void MC_init()
{
    //Initialize the MC layer
    mc_can_init();
    MC_I2C_Init();
    mc_uart_init();
}

void MC_1ms()
{
    //Run any HW specific 1ms cyclic tasks
}

void MC_10ms()
{
    //Run any HW specific 10ms cyclic tasks
}

void MC_100ms()
{
    //Run any HW specific 100ms cyclic tasks
}
