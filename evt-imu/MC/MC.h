/* 
 * File:   MC.h
 *
 * Created on September 8, 2016, 8:07 PM
 */

#ifndef MC_H
#define    MC_H
#include <xc.h>
#include "EVT_std_includes.h"
#include "MC_I2C.h"

void MC_init();
void MC_1ms();
void MC_10ms();
void MC_100ms();

#endif    /* MC_H */
