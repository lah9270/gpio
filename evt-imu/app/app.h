/* 
 * File:   app.h
 *
 * Created on September 8, 2016, 8:07 PM
 */

#ifndef APP_H
#define    APP_H

void app_init();
void app_1ms();
void app_10ms();
void app_100ms();

#endif    /* APP_H */