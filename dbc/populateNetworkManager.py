#!python
"""
    File:    populateNetworkManager.py
    Created: 4/2/2017
"""

import sys
import os
from dbcParser import *


spaces_for_tab = "    "  # 4 spaces instead of tab character
can_msg_structPtr = "io_can_message_S"
codebase_dir = os.path.dirname(os.path.dirname(os.path.abspath( __file__ )))
networkManager_hFile = "IO_networkManager_BASE.h"
networkManager_cFile = "IO_networkManager_BASE.c"
appNetwork_cFile = "app_network_BASE.c"
ioFilePath = os.path.join(codebase_dir, "shared", "IO")
hFilePath = os.path.join(ioFilePath, networkManager_hFile)
cFilePath = os.path.join(ioFilePath, networkManager_cFile)
appFilePath = os.path.join(codebase_dir, "shared", "app", appNetwork_cFile)


def generateEnumDefinition(messages):
    """
    Takes a list of messages and generates an enumerated type.
    """

    type_name = "io_network_message_E"
    typedef_str = "typedef enum " + type_name + "\n{\n"

    for message in messages:
        member_str = spaces_for_tab
        member_str += "IO_NETWORK_" + message.Name().upper()
        member_str += " = " + str(message.CANID()) + ',\n'
        typedef_str += member_str

    closing_str = '} ' + type_name + ";\n"
    typedef_str += closing_str

    return typedef_str


def generateStructDefinition(message):
    """
    Generates a string containing information on the organization of the
    signals as members of a packed bitfield struct.

    Arguments:
     - message: A CANMessage object to generate a struct from.

    Returns:
    A tuple containing a string to be inserted into the network manager files
    and a string of the newly created struct name.
    """

    # start with the top line, easy peasy
    type_name = "io_network_" + message.Name() + "_S"
    typedef_str = "typedef struct " + type_name + "\n{\n"


    for signal in message:
        member_str = spaces_for_tab
        # figure out the type required
        if signal.SignType() == '+' and signal.Length() <= 8:
            member_str += "int8_t "
        elif signal.SignType() == '+' and signal.Length() <= 16:
            member_str += "int16_t "
        elif signal.SignType() == '+':
            member_str += "int32_t "
        elif signal.SignType() == '-' and signal.Length() <= 8:
            member_str += "uint8_t "
        elif signal.SignType() == '-' and signal.Length() <= 16:
            member_str += "uint16_t "
        elif signal.SignType() == '-':
            member_str += "uint32_t "
        else:
            print("The sign of the signal {0} is unknown.", signal.Name())
            member_str += "uint32_t "
        # add signal name, colon, length, semicolon
        member_str += (signal.Name().lower() + ";\n")
        typedef_str += member_str

    typedef_str += "} " + type_name + ";\n"

    return (typedef_str, type_name)


def generatePackagingFunction(message, function_header):
    """
    Generates a function definition and declaration from a CAN message for
    packing data into a CAN data frame.
    """

    func_str = function_header + "\n" + "{" + "\n"

    # let's deal with the message's DLC and ID first
    func_str += spaces_for_tab + "message->numBytes = " + str(message.DLC()) + ";\n"
    func_str += spaces_for_tab + "message->address = " + str(message.CANID()) + ";\n"
    func_str += "\n"

    # now onto the meat of the algorithm
    message.Signals().sort()  # sorts the messages from least to greatest by start bit
    signal_lst = message.Signals()

    bits_left_in_elem = 8
    bits_to_take = 0
    bits_taken = 0
    src_var_name = "contents"
    signal_number = 0
    current_signal = signal_lst[0]
    signal_length = current_signal.Length()
    bits_left_in_signal = signal_length
    op_str = list()

    for i in range(0, message.DLC()):
        op_str = []
        line_str = spaces_for_tab + "message->data[" + str(i) + "] = "
        bits_left_in_elem = 8

        while(bits_left_in_elem > 0):
            if bits_left_in_signal <= bits_left_in_elem:
                op_str += ["(((" + src_var_name + "->" + current_signal.Name().lower() + " >> " + \
                           str(signal_length - bits_left_in_signal) + ")" + " << " + str(8 - bits_left_in_elem) + \
                           "))"]
                bits_left_in_elem -= bits_left_in_signal
                signal_number += 1
                if signal_number == len(signal_lst):
                    break
                current_signal = signal_lst[signal_number]
                signal_length = current_signal.Length()
                bits_left_in_signal = signal_length
            else:
                op_str += ["(((" + src_var_name + "->" + current_signal.Name().lower() + " >> " + \
                          str(signal_length - bits_left_in_signal) + ") & " + getBitMaskString(bits_left_in_elem) + ")" + \
                          " << " + str(8 - bits_left_in_elem) + ")"]
                bits_left_in_signal -= bits_left_in_elem
                bits_left_in_elem = 0

        for j in range(0, len(op_str)):
            line_str += op_str[j]
            if j == len(op_str) - 1:
                line_str += ";\n"
            else:
                line_str += " | "

        func_str += line_str
        line_str = []

    func_str += "}\n"

    return func_str


def generateDepackagingFunction(message, func_call):
    """
    Writes a function to take a CAN frame and extract the data from it into a
    struct.
    """
    pass


def generateCaseStatement(id, func_call):
    """
    Generates the case statement for calling the function.
    """
    case_header = (spaces_for_tab*2) + "case NUMBER:\n" + (spaces_for_tab*2) + "{\n"
    case_header = case_header.replace("NUMBER", str(id))

    casting = "(io_network_" + func_call.lstrip("package_") + "_S *)"
    statement = (spaces_for_tab*3) + func_call + "(" + casting + " data, &message);\n"
    end_case = (spaces_for_tab*3) + "break;\n" + (spaces_for_tab*2) + "}\n"

    return case_header + statement + end_case


def getBitMaskString(n):
    """
    Takes a number n and returns a hexadecimal bitmask to get n LSBs from a
    byte.
    
    Arguments:
     - n (Integer): The number of LSBs to mask off.

    Returns:
    A hexadecimal string for an 8-bit number.
    """
    if n > 8:
        return '0xff'

    number = (2 ** n) - 1
    temp_hex = hex(number).lstrip('0x')
    if len(temp_hex) == 1:
        temp_hex = '0x0' + temp_hex
    else:
        temp_hex = '0x' + temp_hex

    return temp_hex


def addPreprocessorFlag(txNode, code_snippet):
    """
    Surrounds a code snippet in preprocessor conditionals so HEX files don't
    get super bloated.
    """
    ifdef_open_tag = "#ifdef __DEVICE_DEFAULT__"
    ifdef_closing_tag = "#endif /* __DEVICE_DEFAULT__ */"

    new_code = ifdef_open_tag + "\n" + code_snippet + ifdef_closing_tag

    return "\n" + new_code.replace("DEFAULT", txNode.upper()) + "\n"


def main(args):
    """
    Calls the DBC parser and then uses the resulting CANDatabase object to
    generate sections of the Network Manager firmware.
    """
    dbc_file_path = ''

    for arg in args:
        if arg.endswith('.dbc'):
            dbc_file_path = arg
            break

    if dbc_file_path == '':
        print("No file path for the DBC file specified.")
        dbc_file_path = 'EVT_CAN.dbc'

    candb = CANDatabase(dbc_file_path).Load()
    hFile_structs = ""
    cFile_cases = ""
    cFile_definitions = ""
    cFile_declarations = ""
    app_calls_1ms = ""
    app_calls_10ms = ""
    app_calls_100ms = ""
    app_calls_1000ms = ""
    app_declarations = ""

    for message in candb:
        (msg_typedef, msg_type_name) = generateStructDefinition(message)

        packFcn_name = "package_" + message.Name()
        msg_packFcn_declaration = "void " + packFcn_name + "(" + msg_type_name + \
                                  " * contents, " + can_msg_structPtr + \
                                  " * message);\n"
        packFcn_header = msg_packFcn_declaration.rstrip(';\n')
        
        function_def = generatePackagingFunction(message, packFcn_header)

        case_statement = generateCaseStatement(message.CANID(), packFcn_name)
        
        # now add the preprocessor conditional
        node = message.TransmittingNode()
        hFile_structs += addPreprocessorFlag(node, msg_typedef)
        cFile_declarations += addPreprocessorFlag(node, msg_packFcn_declaration)
        cFile_definitions += addPreprocessorFlag(node, function_def)
        cFile_cases += addPreprocessorFlag(node, case_statement)

        for attr in message.Attributes():
            if attr[0] == 'GenMsgCycleTime':
                interval = attr[1]
                func_call = spaces_for_tab*2 + "app_network_" + message.Name() + "();\n"
                function_declaration = "void " + func_call.lstrip(' ')

                # add preprocessor flags to function calls
                func_call = addPreprocessorFlag(message.TransmittingNode(), func_call)

                app_declarations += function_declaration
                if interval == '1':
                    app_calls_1ms += func_call
                elif interval == '10':
                    app_calls_10ms += func_call
                elif interval == '100':
                    app_calls_100ms += func_call
                elif interval == '1000':
                    app_calls_1000ms += func_call

    # generate an enum filled with CAN messages
    hFile_enum = generateEnumDefinition(candb.Messages())

    # populate the header file
    with open(hFilePath, 'r') as file:
        file_text = file.read()

    file_text = file_text.replace("/* GENERATED STRUCTS */", hFile_structs)
    file_text = file_text.replace("/* GENERATED MESSAGES ENUM */", hFile_enum)

    with open(hFilePath.replace("_BASE", ""), 'w') as file:
        file.write(file_text)

    # populate the source code file
    with open(cFilePath, 'r') as file:
        file_text = file.read()

    file_text = file_text.replace("/* GENERATED DECLARATIONS */", cFile_declarations)
    file_text = file_text.replace("/* CASE STATEMENT REPLACEMENT */", cFile_cases)
    file_text = file_text.replace("/* GENERATED FUNCTION DEFINITIONS */", cFile_definitions)

    with open(cFilePath.replace("_BASE", ""), 'w') as file:
        file.write(file_text)

    # populate app_network.c with the period of the message transmits
    with open(appFilePath, 'r') as file:
        file_text = file.read()

    file_text = file_text.replace(spaces_for_tab*2 + "/* GENMSGCYCLETIME = 1 ms */", app_calls_1ms)
    file_text = file_text.replace(spaces_for_tab*2 + "/* GENMSGCYCLETIME = 10 ms */", app_calls_10ms)
    file_text = file_text.replace(spaces_for_tab*2 + "/* GENMSGCYCLETIME = 100 ms */", app_calls_100ms)
    file_text = file_text.replace(spaces_for_tab*2 + "/* GENMSGCYCLETIME = 1000 ms */", app_calls_1000ms)
    file_text = file_text.replace("/* PRIVATE APP DECLARATIONS */", app_declarations)

    with open(appFilePath.replace("_BASE", ""), 'w') as file:
        file.write(file_text)

    print("Code has successfully been generated for the Network Manager!")

    return


if __name__ == "__main__":
    main(sys.argv[1:])
