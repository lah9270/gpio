/* 
 * File:   IO.c
 *
 * Created on September 8, 2016, 8:07 PM
 */

#include "IO.h"
#include "IO_timer.h"
#include "IO_CAN.h"
//#include "IO_UART.h"
#include "IO_SPI.h"
#include "IO_ADS1158.h"
#include "IO_LTC6802.h"
#include "MC_board.h"
#include "IO_networkManager.h"


void IO_init()
{
    //Initialize the IO layer
    io_can_init();
    //io_uart_init();
    /* SPI inits ===============================================================================================================================*/
    io_spi_openCS(MC_BOARD_CS_ADC);
    io_spi_openCS(MC_BOARD_CS_P);

    // SPI_FOSC_8 because the linear chip requires a 1Mhz clock for SPI, and the system clock is 8Mhz. This sets the clock divider to 8
    // mode 3 because this is required by the LTC chip as per the Arduino documentation
    // no reason middle sample was chosen, seems like a middle sample should be reasonable
    io_spi_setRequestedSyncMode(IO_SPI_FOSC_8);
    io_spi_setRequestedBusMode(IO_SPI_MODE_00); // FOR THE ADC: Both 11 and 00 seem to work identically well
    io_spi_setRequestedSampleMode(IO_SPI_SAMPLE_MID);
   
    io_spi_init();
    
    /* END SPI inits ================================================================================================================================ 
     * NOTE: we won't actually do any of this here, it seems like the ADC and LTC modules are actually what will set this up since they need unique special settings
     * This is just here for example for now using LTC settings
     */
    io_networkManager_init();
    io_ads1158_init();

    /* Total boards, board position indexed from 0 (bottom) */
    io_ltc6802_init(4, 0);
    
    io_timer_init(IO_TIMER_4, IO_TIMER_1MS);

}

void IO_1ms()
{
    //Run any IO specific 1ms cyclic tasks
    // io_uart_process();
}

void IO_10ms()
{
    //Run any IO specific 10ms cyclic tasks
    io_ads1158_run();
    io_ltc6802_stateMachine();
}

void IO_100ms()
{
    //Run any IO specific 100ms cyclic tasks
    
}
