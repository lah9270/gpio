/* 
 * File:   config.h
 * Author: derekkg
 *
 * Created on September 20, 2014, 2:16 PM
 */

#ifndef CONFIG_H
#define    CONFIG_H

#include <stdbool.h>


/* Comms Enables ==================================== */
/* Enable prints and debugging over UART*/
#define __UART_EN (0)

#define SPI_EN (1)
#define CAN_EN (1)

/* For now, UART can only be on if SPI is NOT on*/
#define UART_EN (__UART_EN && !SPI_EN)
/* ================================================== */

/* Device =========================================== */
/*Pick a device that the program is running on*/
#define DEV_BOARD (0)
#define BMS_REV_3_0 (1)
/* ================================================== */

#define BALANCE_DISABLE (1)

/* Debug Enables ====================================== */
/* Enable ADC debug print statements */
#define ADC_DEBUG (0)

/* Enable CAN message print statements */
#define MESSAGE_DEBUG (0)

/* Enable CAN Driver debug print statements */
#define CAN_DEBUG (0)
/*====================================================== */

//JOSH JONES: Tune here:
#define DISCHARGE_TIME_MS (45000)// Time (in milisec) a cell should discharge for.
#define THRESHOLD_MV (2)       // Threshold above lowest cell voltage (in mV) that a cell starts discharging at.


//Defines for Battery monitoring chip
#define CELL_COUNT (24)         // //number of cells we are monitoring. This should be a total value (needs to be confirmed, may need to handle on a per-board basis)
#define NUM_BOARDS (2)

#define UNDER_VOLTAGE (3048) //in mV. This is the value that gets sent to the LTC for the under Voltage flags
#define OVER_VOLTAGE (4200) //in mV. This is the value that gets sent to the LTC for the over Voltage flags

#define LTC_DEBUG (0)
#define DEBUG_BMS_CAN (0)

#define LTC6802DASH1 (1)
//end Battery Monitoring chip


//HIGH LOW defines
#define HIGH (1)
#define LOW (0)

// Useage defines for the BMS
#define USART_BAUDRATE (9600)   // Desired BaudRate
#define FREQUENCY (8000000)     // 8Mhz OSC
#define _XTAL_FREQ FREQUENCY    // Needed for __delay_ms()/__delay_us() to work

// Derek: I think James put these in for his unit testing of the BMS
#define SYSTEM_CLOCK_SCALE (256)// Incremenet system clock once for this many CPU ticks
#define CYCLE_TIME_MS (50)      // Time (in milisec) each balancing cycle should take.

#define TICK_TO_MS (1000.0f * SYSTEM_CLOCK_SCALE / FREQUENCY) // Conversion from Ticks to seconds
#define DISCHARGE_TIME_TICK (DISCHARGE_TIME_MS / TICK_TO_MS) // Convert dischage time from ms to ticks at compile time.

//defines for SPI
//SPI_FOSC_8 because the linear chip requires a 1Mhz clock for SPI, and the system clock is 8Mhz. This sets the clock divider to 8
#define SPI_FOSC_8    0b00001010
#define SPI_SYNC_MODE SPI_FOSC_8
#define SPI_MODE MODE_11 // mode 3 because this is required by the BMS as per the arduino documentation
#define SPI_SAMPLE_MODE SMPMID //no reason this was choosen, seems like a middle sample, will need to verify functionality
//end SPI defines

//SPI channel Defines
#define SPI_LINEAR (1)
#define SPI_ADC1 (2)
#define SPI_ADC2 (3)

//CAN Constants
#define MAX_NUM_BYTES 8

#if (BMS_REV_3_0)
#define ALT_CAN_PINS (1)
#else
#define ALT_CAN_PINS (0)
#endif


// PIC18F25K80 Configuration Bit Settings

// 'C' source line config statements
#ifndef UNIT_TESTING

#include <xc.h>

#endif

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

// CONFIG1L
#pragma config RETEN = OFF      // VREG Sleep Enable bit (Ultra low-power regulator is Disabled (Controlled by REGSLP bit))
#pragma config INTOSCSEL = HIGH // LF-INTOSC Low-power Enable bit (LF-INTOSC in High-power mode during Sleep)
#pragma config SOSCSEL = HIGH   // SOSC Power Selection and mode Configuration bits (High Power SOSC circuit selected)
#pragma config XINST = OFF      // Extended Instruction Set (Disabled)

// CONFIG1H
#pragma config FOSC = INTIO2    // Oscillator (Internal RC oscillator)
#pragma config PLLCFG = OFF     // PLL x4 Enable bit (Disabled)
#pragma config FCMEN = OFF      // Fail-Safe Clock Monitor (Disabled)
#pragma config IESO = OFF       // Internal External Oscillator Switch Over Mode (Disabled)

// CONFIG2L
#pragma config PWRTEN = ON      // Power Up Timer (Enabled)
#pragma config BOREN = SBORDIS  // Brown Out Detect (Enabled in hardware, SBOREN disabled)
#pragma config BORV = 3         // Brown-out Reset Voltage bits (1.8V)
#pragma config BORPWR = ZPBORMV // BORMV Power level (ZPBORMV instead of BORMV is selected)

// CONFIG2H
#pragma config WDTEN = OFF      // Watchdog Timer (WDT disabled in hardware; SWDTEN bit disabled)
#pragma config WDTPS = 1048576  // Watchdog Postscaler (1:1048576)

// CONFIG3H

#if (ALT_CAN_PINS)
#pragma config CANMX = PORTC    // ECAN Mux bit (ECAN TX and RX pins are located on RC6 and RC7, respectively)
#else
#pragma config CANMX = PORTB    // ECAN Mux bit (ECAN TX and RX pins are located on RB2 and RB3, respectively)
#endif

#pragma config MSSPMSK = MSK7   // MSSP address masking (7 Bit address masking mode)
#pragma config MCLRE = ON       // Master Clear Enable (MCLR Enabled, RE3 Disabled)

// CONFIG4L
#pragma config STVREN = ON      // Stack Overflow Reset (Enabled)
#pragma config BBSIZ = BB2K     // Boot Block Size (2K word Boot Block size)

// CONFIG5L
#pragma config CP0 = OFF        // Code Protect 00800-01FFF (Disabled)
#pragma config CP1 = OFF        // Code Protect 02000-03FFF (Disabled)
#pragma config CP2 = OFF        // Code Protect 04000-05FFF (Disabled)
#pragma config CP3 = OFF        // Code Protect 06000-07FFF (Disabled)

// CONFIG5H
#pragma config CPB = OFF        // Code Protect Boot (Disabled)
#pragma config CPD = OFF        // Data EE Read Protect (Disabled)

// CONFIG6L
#pragma config WRT0 = OFF       // Table Write Protect 00800-03FFF (Disabled)
#pragma config WRT1 = OFF       // Table Write Protect 04000-07FFF (Disabled)
#pragma config WRT2 = OFF       // Table Write Protect 08000-0BFFF (Disabled)
#pragma config WRT3 = OFF       // Table Write Protect 0C000-0FFFF (Disabled)

// CONFIG6H
#pragma config WRTC = OFF       // Config. Write Protect (Disabled)
#pragma config WRTB = OFF       // Table Write Protect Boot (Disabled)
#pragma config WRTD = OFF       // Data EE Write Protect (Disabled)

// CONFIG7L
#pragma config EBTR0 = OFF      // Table Read Protect 00800-03FFF (Disabled)
#pragma config EBTR1 = OFF      // Table Read Protect 04000-07FFF (Disabled)
#pragma config EBTR2 = OFF      // Table Read Protect 08000-0BFFF (Disabled)
#pragma config EBTR3 = OFF      // Table Read Protect 0C000-0FFFF (Disabled)

// CONFIG7H
#pragma config EBTRB = OFF      // Table Read Protect Boot (Disabled)

#endif    /* CONFIG_H */

