/* 
 * File:   MC.c
 * Author: Derek Gutheil <dkg8689@rit.edu>
 *
 * Created on September 8, 2016, 8:07 PM
 */

#include "MC.h"
#include "MC_CAN.h"
#include "MC_UART.h"
#include "MC_SPI.h"

void MC_init()
{
    //Initialize the HW layer
    mc_can_init();
    mc_uart_init();
    mc_spi_open();
}

void MC_1ms()
{
    //Run any HW specific 1ms cyclic tasks
}

void MC_10ms()
{
    //Run any HW specific 10ms cyclic tasks
}

void MC_100ms()
{
    //Run any HW specific 100ms cyclic tasks
}
