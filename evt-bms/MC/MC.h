/* 
 * File:   MC.h
 * Author: Derek Gutheil <dkg8689@rit.edu>
 *
 * Created on September 8, 2016, 8:07 PM
 */

#ifndef MC_H
#define    MC_H

void MC_init();
void MC_1ms();
void MC_10ms();
void MC_100ms();

#endif    /* MC_H */