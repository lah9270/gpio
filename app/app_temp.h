
#ifndef APP_TEMP_H
#define	APP_TEMP_H

#include "EVT_std_includes.h"

bool app_temp_dataReady();
void app_temp_updateTemps();
uint8_t *app_temp_getCellTemps();

#endif	/* APP_TEMP_H */

