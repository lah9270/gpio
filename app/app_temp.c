#include "EVT_std_debug.h"
#include "app_temp.h"
#include "IO_ADS1158.h"

#define TEMP_MIN        (0)
#define TEMP_MAX        (100)
#define TEMP_NUM_CELLS  (15)

static bool temp_newData = false;

#ifdef calc_resistance
/* Use this if we want to calculate the resistance of the thermistor and then
 * lookup the temperature based on the resistance.*/
static const uint16_t temp_LUT[] = {    28903, 27606, 26375, 25208, 24101,
                                        23050, 22051, 21103, 20201, 19344,
                                        18529, 17754, 17016, 16314, 15645,
                                        15009, 14402, 13823, 13272, 12746,
                                        12244, 11766, 11309, 10873, 10456,
                                        10058,  9678,  9314,  8967,  8634,
                                         8315,  8012,  7720,  7441,  7174,
                                         6918,  6673,  6438,  6212,  5996,
                                         5789,  5590,  5399,  5216,  5040,
                                         4872,  4709,  4554,  4404,  4260,
                                         4121,  3988,  3860,  3737,  3619,
                                         3505,  3395,  3289,  3187,  3089,
                                         2995,  2904,  2816,  2731,  2650,
                                         2571,  2495,  2422,  2351,  2283,
                                         2217,  2153,  2092,  2033,  1975,
                                         1920,  1866,  1815,  1764,  1716,
                                         1669,  1624,  1580,  1538,  1497,
                                         1457,  1419,  1382,  1346,  1311,
                                         1277,  1244,  1212,  1182,  1152,
                                         1123,  1095,  1068,  1041,  1016, 991};
#endif

/* Use this table if we're looking the temp values based directly on the 
 * voltages from the ADC. */
static const uint16_t temp_LUT[] = {    3715, 3670, 3625, 3580, 3534,
                                        3487, 3440, 3392, 3344, 3296,
                                        3247, 3198, 3149, 3100, 3050,
                                        3001, 2951, 2901, 2851, 2802,
                                        2752, 2703, 2654, 2605, 2556,
                                        2507, 2459, 2411, 2364, 2317,
                                        2270, 2224, 2178, 2133, 2089,
                                        2044, 2001, 1958, 1916, 1874,
                                        1833, 1793, 1753, 1714, 1676,
                                        1638, 1601, 1565, 1529, 1494,
                                        1459, 1426, 1392, 1360, 1329,
                                        1298, 1267, 1237, 1208, 1180,
                                        1152, 1125, 1099, 1073, 1047,
                                        1023,  998,  975,  952,  929,
                                         907,  886,  865,  845,  825,
                                         805,  786,  768,  750,  732,
                                         715,  699,  682,  666,  651,
                                         636,  621,  607,  593,  580,
                                         566,  553,  540,  529,  516,
                                         505,  493,  482,  471,  461,
                                         451 };

static uint8_t temp_cellTemps[15] = {   0, 0, 0, 0, 0,
                                        0, 0, 0, 0, 0,
                                        0, 0, 0, 0, 0 };

static uint8_t temp_calcTemp(uint16_t mVolts)
{
    int8_t direction;
    uint8_t index; 

    /* Start in the middle of the table to save time*/
    index = (sizeof(temp_LUT)/sizeof(uint16_t))/2;
    
    /* See if our voltage is greater than the median value
     * in order to determine if we want to look in the upper half
     * of the array or the lower half.
     */
    if (temp_LUT[index] < mVolts)
    {
        /* The temperature we want is less than 50 degrees C */
        direction = -1;
    }
    else
    {
        /* The temperature we want is greater than 50 degrees C*/
        direction = 1;
    }
    
    while ((TEMP_MIN < index) && (index < TEMP_MAX))
    {
        if ((mVolts > temp_LUT[index]) && (direction == 1))
        {
            /* If we're moving up in the array and our voltage is 
             * greater than the array value, then we're done. */
            break;
        }
        else if ((mVolts < temp_LUT[index]) && (direction == -1))
        {
            /* If we're moving down in the array and our voltage
             * is less than the array value, then we're done. */
            break;
        }
        
        index += direction;
    }
    
    return index;
}

bool app_temp_dataReady()
{
    return temp_newData;
}

void app_temp_updateTemps()
{
    static uint8_t counts = 0;
    uint16_t *tmpmVolts;
    uint8_t cellIndex;

    if (counts == 0)
    {
        io_ads1158_changePrecision(ADS1158_READ_PRECISION_MILLIVOLTS);
    } 

    else if (counts == 1)
    {
        tmpmVolts = io_ads1158_getMilliVoltChannels();
    
        /* Loop through all the cells */
        for(cellIndex = 0; cellIndex != TEMP_NUM_CELLS; ++cellIndex)
        {
            /* Load the array with the temperatures */
            temp_cellTemps[cellIndex] = temp_calcTemp(tmpmVolts[cellIndex+ ADS1158_CHANNEL_ID_AIN1]);
        }
    
        /* Indicate that we now have fresh data */
        temp_newData = true;
    }

    /* Update the counter in a cyclic manner */
    counts = (counts + 1) % 10;
}

uint8_t *app_temp_getCellTemps()
{
    temp_newData = false;
    return temp_cellTemps;
}
