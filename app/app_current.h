/* 
 * File:   app_current.h
 * Author: braeden
 *
 * Created on April 21, 2017, 8:10 PM
 */

#ifndef APP_CURRENT_H
#define	APP_CURRENT_H
#include "EVT_std_includes.h"

void app_current_calcCurrent();
int16_t app_current_getCurrent();

#endif	/* APP_CURRENT_H */

