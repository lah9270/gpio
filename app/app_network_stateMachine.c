/* 
 * File:   app_network_stateMachine.c
 * Author: Derek Gutheil <dkg8689@rit.edu>
 *
 * Created on October 8, 2016
 */

#include "app_network_stateMachine.h"
#include "app_network.h"
 
/* ===========================================================================================================================
 * Private module variables
 * ===========================================================================================================================
*/
app_network_sm_state_E networkSMCurrentState = APP_NETWORK_SM_STATE_INIT;
app_network_sm_state_E networkSMNextState = APP_NETWORK_SM_STATE_INIT;
app_network_sm_transition_E networkSMTransitionReason;
bool networkSMStateChanged = false;

/* ===========================================================================================================================
 * Private module function declarations
 * ===========================================================================================================================
*/
void app_network_sm_enterState();
void app_network_sm_processState();
void app_network_sm_exitState();

/* ===========================================================================================================================
 *  Module function definitions
 * ===========================================================================================================================
*/

void app_network_sm()
{
    
    //entry functions --------------------------------------------------------------
    if (networkSMStateChanged)
    {
        app_network_sm_enterState();
        networkSMStateChanged = false;
    }
    else
    {
        // There was no state change, don't run the entry functions
    }

    //periodic functions -----------------------------------------------------------
    app_network_sm_processState();
    

    //exit functions ---------------------------------------------------------------
    if (networkSMStateChanged)
    {
        app_network_sm_exitState();
    }
    else
    {
        // There was no state change, don't run the exit functions
    }
    
}


void app_network_sm_enterState()
{
    //kick off any asynchronous tasks for each state
    switch(networkSMCurrentState)
    {
        case APP_NETWORK_SM_STATE_INIT:
            break;
        case APP_NETWORK_SM_STATE_ISOLATED:
            //start listening for GTW? might have already happened
            break;
        case APP_NETWORK_SM_STATE_NETWORKED:
            app_network_beginTransmitting();
            break;
        case APP_NETWORK_SM_STATE_QUIET:
            app_network_stopTransmitting();
            break;

        /*
         * We should never get here
        */
        default:
            break;
    }
}


void app_network_sm_processState()
{
    
    switch (networkSMCurrentState)
    {
        case APP_NETWORK_SM_STATE_INIT:
            
            //check to see if the init functions are done
            if (app_network_getInitSuccess())
            {
                //This state is over, lets get out of here!
                networkSMNextState = APP_NETWORK_SM_STATE_ISOLATED;
                
                //this is just for debugging purposes, serves no practical purpose
                networkSMTransitionReason = APP_NETWORK_SM_TRANSITION_INIT_SUCCESS;
            }
            break;
            
        case APP_NETWORK_SM_STATE_ISOLATED:
            
            //check to see if we have heard from the GTW
            if (app_network_isGTWPresent())
            {
                //This state is over, lets get out of here!
                networkSMNextState = APP_NETWORK_SM_STATE_NEGOTIATIONS;
                
                //this is just for debugging purposes, serves no practical purpose
                networkSMTransitionReason = APP_NETWORK_SM_TRANSITION_GTW_CONT;
            }
            break;
            
        case APP_NETWORK_SM_STATE_NEGOTIATIONS:
            
            //check to see if the GTW has told us to start transmitting
            if (app_network_getTransmitRequestStatus())
            {
                //This state is over, lets get out of here!
                networkSMNextState = APP_NETWORK_SM_STATE_NETWORKED;
                
                //this is just for debugging purposes, serves no practical purpose
                networkSMTransitionReason = APP_NETWORK_SM_TRANSITION_GTW_TRANS;
            }
            else if (app_network_getQuietRequestStatus())
            {
                //This state is over, lets get out of here!
                networkSMNextState = APP_NETWORK_SM_STATE_QUIET;
                
                //this is just for debugging purposes, serves no practical purpose
                networkSMTransitionReason = APP_NETWORK_SM_TRANSITION_GTW_QUIET;
            }
            else if (app_network_isGTWPresent() == false)
            {
                //GTW has left the bus, lets go back to isolated
                networkSMNextState = APP_NETWORK_SM_STATE_ISOLATED;
                
                //this is just for debugging purposes, serves no practical purpose
                networkSMTransitionReason = APP_NETWORK_SM_TRANSITION_TIME_OUT;
            }
            break;
            
        case APP_NETWORK_SM_STATE_NETWORKED:
            
            //check to see if the GTW has told us to go quiet
            if (app_network_getQuietRequestStatus())
            {
                //This state is over, lets get out of here!
                networkSMNextState = APP_NETWORK_SM_STATE_QUIET;
                
                //this is just for debugging purposes, serves no practical purpose
                networkSMTransitionReason = APP_NETWORK_SM_TRANSITION_GTW_QUIET;
            }
            else if (app_network_isGTWPresent() == false)
            {
                //GTW has left the bus, lets go back to isolated
                networkSMNextState = APP_NETWORK_SM_STATE_ISOLATED;
                
                //this is just for debugging purposes, serves no practical purpose
                networkSMTransitionReason = APP_NETWORK_SM_TRANSITION_TIME_OUT;
            }
            break;

        case APP_NETWORK_SM_STATE_QUIET:
            
            //check to see if the GTW has told us to start transmitting
            if (app_network_getTransmitRequestStatus())
            {
                //This state is over, lets get out of here!
                networkSMNextState = APP_NETWORK_SM_STATE_NETWORKED;
                
                //this is just for debugging purposes, serves no practical purpose
                networkSMTransitionReason = APP_NETWORK_SM_TRANSITION_GTW_TRANS;
            }
            else if (app_network_isGTWPresent() == false)
            {
                //GTW has left the bus, lets go back to isolated
                networkSMNextState = APP_NETWORK_SM_STATE_ISOLATED;
                
                //this is just for debugging purposes, serves no practical purpose
                networkSMTransitionReason = APP_NETWORK_SM_TRANSITION_TIME_OUT;
            }
            break;
            
        default:
            /*
             * We should never get here, if we do we set the state enum to an illegal value, reset it to a safe state
            */
            break;
    }
    
    //if we changed the state, make sure we let the exit and entry functions know!
    if (networkSMCurrentState != networkSMNextState)
    {
        networkSMStateChanged = true;
    }
    
}

void app_network_sm_exitState()
{
    switch(networkSMCurrentState)
    {
        case APP_NETWORK_SM_STATE_INIT:

            break;
        case APP_NETWORK_SM_STATE_ISOLATED:

            break;
        case APP_NETWORK_SM_STATE_NEGOTIATIONS:

            break;
        case APP_NETWORK_SM_STATE_NETWORKED:

            break;
        case APP_NETWORK_SM_STATE_QUIET:

            break;

        /*
         * We should never get here
        */
        default:
            break;
    }
    
    
    //update the current state to the next desired state!
    networkSMCurrentState = networkSMNextState;
}




//a couple of getters
//might get rid of these
app_network_sm_state_E app_network_sm_getState()
{
    return networkSMCurrentState;
}

app_network_sm_transition_E app_network_sm_getTransition()
{
    return networkSMTransitionReason;
}