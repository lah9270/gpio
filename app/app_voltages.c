/*************************************
 *  File - app_voltages.c
 *
 *  Date - 4/21/2017
 *  Author - John Jenco
 *************************************/
 
 
#include "app_voltages.h"
#include "IO_LTC6802.h"

static uint16_t voltageArray[IO_LTC6802_MAX_NUM_BOARDS * IO_LTC6802_BYTES_PER_BOARD];

/* Get all voltage measurements from battery cells and store them locally */
void app_voltages_updateCellVoltages()
{
    uint8_t i;
    io_ltc6802_cellV *tempArray = io_ltc6802_getCellVoltages();
    
    for(i = 0; i < (IO_LTC6802_MAX_NUM_BOARDS * IO_LTC6802_BYTES_PER_BOARD); i++)
    {
        voltageArray[i] = tempArray[i].value;
    }
    
}

/* For network app stuffs */
uint16_t *app_voltages_getCellVoltages()
{
    return voltageArray;
}

