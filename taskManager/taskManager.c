/*
 * File:   taskManager.c
 * Author: Derek Gutheil <dkg8689@rit.edu>
 *
 * Created on September 8, 2016
 */
#include "taskManager.h"
#include "interruptHandler.h"
#include "MC.h"
#include "IO.h"
#include "dev.h"
#include "app.h"

unsigned long taskManagerTimerMS = 0; //will overflow in 49.7 days

void taskManager_init()
{
    app_init();
    dev_init();
    IO_init();
    MC_init();
    init_interruptHandler();
}

void taskManager()
{
    ++taskManagerTimerMS;
    if ( ! (taskManagerTimerMS % 1 ) )
    {
        //switch case to get to a 1ms period
        MC_1ms();
        IO_1ms();
        dev_1ms();
        app_1ms();
    }
    
    if ( ! (taskManagerTimerMS % 10 ) )
    {
        //switch case to get to a 10ms period
        MC_10ms();
        IO_10ms();
        dev_10ms();
        app_10ms();
    }
    
    if ( ! (taskManagerTimerMS % 100 ) )
    {
        //switch case to get to a 100ms period
        MC_100ms();
        IO_100ms();
        dev_100ms();
        app_100ms();
    }
    
    if ( ! (taskManagerTimerMS % 1000 ) )
    {
        //switch case to get to a 1000ms period
        app_1000ms();
    }
}